package br.com.mercadodemusica;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration 
@EnableTransactionManagement
public class PersistenceConfig {

	private static Properties prop = new Properties();
	
	public PersistenceConfig() throws IOException {
		
		FileInputStream input = new FileInputStream("/properties/database-config.properties");
		prop.load(input);
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws IOException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      
//	      em.setPersistenceProviderClass(PersistenceProvider.class);
 
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { "br.com.mercadodemusica.entities" });
 
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);	
		em.setJpaProperties(additionalProperties());
 
		return em;
   }
	   
   @Bean
   public DataSource dataSource() throws IOException{
		   
	   DriverManagerDataSource dataSource = new DriverManagerDataSource();
	   dataSource.setDriverClassName(prop.getProperty("driver"));
	   dataSource.setUrl(prop.getProperty("url"));
	   dataSource.setUsername(prop.getProperty("username"));
	   dataSource.setPassword(prop.getProperty("password"));
	   return dataSource;
   }
	   
	      
   @Bean
   public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
	   JpaTransactionManager transactionManager = new JpaTransactionManager();
	   transactionManager.setEntityManagerFactory(emf);
 
	   return transactionManager;
   }
 
   @Bean
   public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
	   return new PersistenceExceptionTranslationPostProcessor();
   }
	 
	   
	   
   private Properties additionalProperties() throws IOException {
      
	   Properties properties = new Properties();
	   properties.setProperty("hibernate.hbm2ddl.auto", prop.getProperty("hbm2ddl"));
	   properties.setProperty("hibernate.dialect", prop.getProperty("dialect"));
	   properties.setProperty("hibernate.show_sql", prop.getProperty("show_sql"));
	   return properties;
   }

	
}
