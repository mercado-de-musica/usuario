package br.com.mercadodemusica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.bedatadriven.jackson.datatype.jts.JtsModule;

import br.com.mercadodemusica.serializersDeserializers.CalendarDeserializer;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class UsuarioApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(UsuarioApplication.class, args);
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UsuarioApplication.class);
    }
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
	
	
	@Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        
        Properties props = new Properties();
//        props.put("mail.smtp.host", "smtp.gmail.comsmtp.gmail.com");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        
        javaMailSender.setJavaMailProperties(props);
        
        javaMailSender.setUsername("mercadodemusica@mercadodemusica.com.br");
        javaMailSender.setPassword("gibson100");
        

        return javaMailSender;
    }
	
	@Bean
	public Mapper dozerBeanMapper(){
		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		List<String> listaDeXmls = new ArrayList<String>();
		listaDeXmls.add("dozerMapping.xml");
		
		dozerBeanMapper.setMappingFiles(listaDeXmls);
		
		return dozerBeanMapper;
	}
	
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer addCustomBigDecimalDeserialization() {
        return new Jackson2ObjectMapperBuilderCustomizer() {

            @Override
            public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
                jacksonObjectMapperBuilder.deserializerByType(Calendar.class, new CalendarDeserializer());
            }

        };
    }
    
    @Bean
    public JtsModule jtsModule() {
        return new JtsModule();
    }
}
