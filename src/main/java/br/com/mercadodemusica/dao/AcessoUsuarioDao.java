package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.Usuario;

public interface AcessoUsuarioDao extends Dao<AcessoUsuario> {

	AcessoUsuario obterPorUsuario(Usuario usuario);

	AcessoUsuario obterPorEmail(String email);
}
