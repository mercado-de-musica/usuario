package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Cidade;
import br.com.mercadodemusica.entities.Endereco;
import br.com.mercadodemusica.entities.Estado;
import br.com.mercadodemusica.entities.Pais;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.enums.PaisesEnum;

public interface EnderecoDao  extends Dao<Endereco> {

	Cidade obterCidadePorNome(String nome);

	Estado obterEstadoPorNome(String nome);

	List<Endereco> obterEnderecosPorUsuario(Usuario usuario);

	Pais obterPaisPorEnum(PaisesEnum paisEnum);
}
