package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.InteresseLead;

public interface InteresseLeadDao extends Dao<InteresseLead>{

	List<InteresseLead> obterPorNomeAndLead(InteresseLead interesseLead);

}
