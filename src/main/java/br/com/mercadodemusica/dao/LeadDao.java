package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Lead;

public interface LeadDao extends Dao<Lead> {

	Lead obterPorEmail(String email);

}
