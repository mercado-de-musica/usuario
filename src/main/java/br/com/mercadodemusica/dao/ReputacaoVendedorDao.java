package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Produto;
import br.com.mercadodemusica.entities.ReputacaoVendedor;
import br.com.mercadodemusica.entities.Usuario;

public interface ReputacaoVendedorDao extends Dao<ReputacaoVendedor>{

	List<ReputacaoVendedor> obterReputacaoPorUsuario(Usuario usuario);

	ReputacaoVendedor obterReputacaoPorProdutoCompradoCompradorVendedor(Produto produto, Usuario usuarioComprador);

}
