package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Telefone;
import br.com.mercadodemusica.entities.Usuario;

public interface TelefoneDao extends Dao<Telefone> {

	List<Telefone> obterTelefonesPorUsuario(Usuario usuario);

}
