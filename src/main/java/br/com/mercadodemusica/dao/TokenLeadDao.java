package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.TokenLead;

public interface TokenLeadDao extends Dao<TokenLead>{

	TokenLead obterPorToken(String token);

}
