package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.TokenLiberacaoUsuario;

public interface TokenLiberacaoUsuarioDao extends Dao<TokenLiberacaoUsuario> {

	TokenLiberacaoUsuario obterPorToken(String token);

}
