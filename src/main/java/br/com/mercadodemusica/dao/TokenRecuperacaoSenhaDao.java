package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.TokenRecuperacaoSenha;

public interface TokenRecuperacaoSenhaDao  extends Dao<TokenRecuperacaoSenha> {

	TokenRecuperacaoSenha trazerDadosViaToken(TokenRecuperacaoSenha tokenRecuperacaoDeSenha);

	List<TokenRecuperacaoSenha> obterTokensGeradosAposUmaHora(TokenRecuperacaoSenha tokenRecuperacaoDeSenha);

}
