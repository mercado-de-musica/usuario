package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.UsuarioCrawler;

public interface UsuarioCrawlerDao extends Dao<UsuarioCrawler> {

}
