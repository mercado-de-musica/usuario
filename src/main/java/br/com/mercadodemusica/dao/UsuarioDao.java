package br.com.mercadodemusica.dao;

import java.math.BigInteger;
import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Endereco;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;

public interface UsuarioDao extends Dao<Usuario> {
	
	List<BigInteger> obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, Integer quantidadeRegistros, Integer pagina , String nome) throws RegraDeNegocioException;

	List<BigInteger> obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, Integer quantidadeDeRegistro, Integer pagina, String nome) throws RegraDeNegocioException;

	List<BigInteger> obterIdsUsuariosComProdutos(Integer quantidadeRegistros, Integer pagina, String nome) throws RegraDeNegocioException;

	BigInteger obterContagemUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, String nome) throws RegraDeNegocioException;

	BigInteger obterContagemUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, String nome) throws RegraDeNegocioException;

	BigInteger obterContagemUsuariosComProdutos(String nome) throws RegraDeNegocioException;
	
}
