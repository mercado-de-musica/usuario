package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.entities.UsuarioPessoaFisica;

public interface UsuarioPessoaFisicaDao extends Dao<UsuarioPessoaFisica> {
	
	UsuarioPessoaFisica selectPorCpf(String cpf);

	UsuarioPessoaFisica selectPorUsuario(Usuario usuario);
}
