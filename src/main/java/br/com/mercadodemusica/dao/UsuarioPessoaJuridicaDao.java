package br.com.mercadodemusica.dao;

import java.math.BigInteger;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.entities.UsuarioPessoaJuridica;

public interface UsuarioPessoaJuridicaDao extends Dao<UsuarioPessoaJuridica> {

	UsuarioPessoaJuridica selectPorCnpj(String cnpj);

	UsuarioPessoaJuridica selectPorUsuario(Usuario usuario);

	UsuarioPessoaJuridica selectPorCpfTitular(String cpfTitular);

	UsuarioPessoaJuridica obterUsuarioPessoaJuridicaPorId(BigInteger id);

}
