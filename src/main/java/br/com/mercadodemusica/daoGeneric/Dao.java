package br.com.mercadodemusica.daoGeneric;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

public interface Dao<T> extends Serializable {
	
	T save(T t);
	
	T update(T t);
	
	void delete(T t);
	
	public List<T> selectAll();
	
	public T selectOne(BigInteger id);
	
	void flush();
	
	void clear();
	
	void detachEntidade(T t);
	
	Boolean contains(T t);
	
	void refresh(T t);
	
	void lockPessimisticWrite(T t);
	
	void lockOptimisc(T t);
	
	public void merge(T t);
}
