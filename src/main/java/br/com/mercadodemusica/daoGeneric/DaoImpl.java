package br.com.mercadodemusica.daoGeneric;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public abstract class DaoImpl<T> implements Dao<T>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Class<T> entityClass;
	
	public DaoImpl(Class<T> clazzToSet) {
		this.entityClass = clazzToSet;
	}	
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<T> selectAll() {
		return entityManager.createQuery( "from " + entityClass.getName() ).getResultList();
	}
	
	public T selectOne(BigInteger id) {
		return entityManager.find( entityClass, id );
	}
	
	public T save(T t) {	
		entityManager.persist(t);
		return t;
	}
	
	public T update(T t) {
		return entityManager.merge(t);
	}

	public void delete(T t) {
		entityManager.remove(t);
	}

	public void flush() {
		entityManager.flush();
	}
	
	public void clear() {
		entityManager.clear();
	}
	
	public void close() {
		entityManager.close();
	}
	
	public void detachEntidade(T t) {
		entityManager.detach(t);
	}
	
	public Boolean contains(T t) {
		return entityManager.contains(t);
	}
	
	public void refresh(T t) {
		entityManager.refresh(t);
	}
	
	public void lockPessimisticWrite(T t) {
		entityManager.lock(t, LockModeType.PESSIMISTIC_WRITE);
	}
	
	public void lockOptimisc(T t) {
		entityManager.lock(t, LockModeType.OPTIMISTIC);
	}
	
	public void merge(T t) {
		entityManager.merge(t);
	}
}	


