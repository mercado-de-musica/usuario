package br.com.mercadodemusica.daoImpl;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.Usuario;

@Repository
public class AcessoUsuarioDaoImpl extends DaoImpl<AcessoUsuario> implements AcessoUsuarioDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AcessoUsuarioDaoImpl() {
		super(AcessoUsuario.class);
	}

	@Override
	public AcessoUsuario obterPorUsuario(Usuario usuario) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AcessoUsuario> cq = cb.createQuery(AcessoUsuario.class);
		Root<AcessoUsuario> acessoUsuarioRoot = cq.from(AcessoUsuario.class);
		
		cq.where(cb.equal(acessoUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario")));
		cq.select(acessoUsuarioRoot);
		TypedQuery<AcessoUsuario> q = entityManager.createQuery(cq);
		q.setParameter("usuario", usuario);
		
		q.setMaxResults(1);
		
		return q.getSingleResult();
	}
	
	@Override
	public AcessoUsuario obterPorEmail(String email) {
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<AcessoUsuario> cq = cb.createQuery(AcessoUsuario.class);
			Root<AcessoUsuario> acessoUsuarioRoot = cq.from(AcessoUsuario.class);
			
			cq.where(cb.equal(acessoUsuarioRoot.<String>get("email"),cb.parameter(String.class, "email")));
			cq.select(acessoUsuarioRoot);
			TypedQuery<AcessoUsuario> q = entityManager.createQuery(cq);
			q.setParameter("email", email);
			
			q.setMaxResults(1);
			
			return q.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
		
	}

}
