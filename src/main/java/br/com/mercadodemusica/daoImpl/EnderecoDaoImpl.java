package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.EnderecoDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Cidade;
import br.com.mercadodemusica.entities.Endereco;
import br.com.mercadodemusica.entities.Estado;
import br.com.mercadodemusica.entities.Pais;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.enums.PaisesEnum;

@Repository
public class EnderecoDaoImpl extends DaoImpl<Endereco> implements EnderecoDao {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EnderecoDaoImpl() {
		super(Endereco.class);
	}

	public Cidade obterCidadePorNome(String nome) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Cidade> cq = cb.createQuery(Cidade.class);
		Root<Cidade> cidade = cq.from(Cidade.class);
		
		cq.where(cb.like(cidade.<String>get("nome"),cb.parameter(String.class, "nome")));
		cq.select(cidade);
		TypedQuery<Cidade> q = entityManager.createQuery(cq);
		q.setParameter("nome", nome);
		
		List<Cidade> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	public Estado obterEstadoPorNome(String nome) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Estado> cq = cb.createQuery(Estado.class);
		Root<Estado> estado = cq.from(Estado.class);
		
		cq.where(cb.like(estado.<String>get("nome"),cb.parameter(String.class, "nome")));
		cq.select(estado);
		TypedQuery<Estado> q = entityManager.createQuery(cq);
		q.setParameter("nome", nome);
		
		List<Estado> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	public Pais obterPaisPorEnum(PaisesEnum paisEnum) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Pais> cq = cb.createQuery(Pais.class);
		Root<Pais> pais = cq.from(Pais.class);
		
		cq.where(cb.equal(pais.<PaisesEnum>get("paisEnum"),cb.parameter(PaisesEnum.class, "paisEnum")));
		cq.select(pais);
		TypedQuery<Pais> q = entityManager.createQuery(cq);
		q.setParameter("paisEnum", paisEnum);
		
		List<Pais> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	public List<Endereco> obterEnderecosPorUsuario(Usuario usuario) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Endereco> cq = cb.createQuery(Endereco.class);
		Root<Endereco> endereco = cq.from(Endereco.class);
		
		cq.where(cb.equal(endereco.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario")));
		cq.select(endereco);
		TypedQuery<Endereco> q = entityManager.createQuery(cq);
		q.setParameter("usuario", usuario);
				
		return q.getResultList();
		
	}	
}
