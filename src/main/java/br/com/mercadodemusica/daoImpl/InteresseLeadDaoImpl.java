package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.InteresseLeadDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.InteresseLead;
import br.com.mercadodemusica.entities.Lead;

@Repository
public class InteresseLeadDaoImpl extends DaoImpl<InteresseLead> implements InteresseLeadDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InteresseLeadDaoImpl() {
		super(InteresseLead.class);
	}

	@Override
	@Transactional(readOnly = true)
	public List<InteresseLead> obterPorNomeAndLead(InteresseLead interesseLead) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<InteresseLead> cq = cb.createQuery(InteresseLead.class);
		Root<InteresseLead> InteresseLead = cq.from(InteresseLead.class);
		
//		cq.where(cb.like(InteresseLead.<String>get("nomeInteresse"),cb.parameter(String.class, "nomeInteresse")));
		cq.where(cb.equal(InteresseLead.<Lead>get("lead"),cb.parameter(Lead.class, "lead")));
		
		TypedQuery<InteresseLead> q = entityManager.createQuery(cq);
		q.setParameter("lead", interesseLead.getLead());
//		q.setParameter("nomeInteresse", interesseLead.getNomeInteresse());
		
		
		return q.getResultList();
		
	}
}
