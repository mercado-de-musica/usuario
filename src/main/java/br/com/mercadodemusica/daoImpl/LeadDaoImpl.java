package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.LeadDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Lead;

@Repository
public class LeadDaoImpl extends DaoImpl<Lead> implements LeadDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LeadDaoImpl() {
		super(Lead.class);
	}

	@Override
	@Transactional(readOnly = true)
	public Lead obterPorEmail(String email) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Lead> cq = cb.createQuery(Lead.class);
		Root<Lead> Lead = cq.from(Lead.class);
		
		cq.where(cb.like(Lead.<String>get("email"),cb.parameter(String.class, "email")));
		
		TypedQuery<Lead> q = entityManager.createQuery(cq);
		q.setParameter("email", email);
		
		List<Lead> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

}
