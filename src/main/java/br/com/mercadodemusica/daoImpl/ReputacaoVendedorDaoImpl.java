package br.com.mercadodemusica.daoImpl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.ReputacaoVendedorDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Produto;
import br.com.mercadodemusica.entities.ReputacaoVendedor;
import br.com.mercadodemusica.entities.Usuario;

@Repository
public class ReputacaoVendedorDaoImpl extends DaoImpl<ReputacaoVendedor> implements ReputacaoVendedorDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3200901664832891257L;

	public ReputacaoVendedorDaoImpl() {
		super(ReputacaoVendedor.class);
	}

	@Override
	public List<ReputacaoVendedor> obterReputacaoPorUsuario(Usuario usuario) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ReputacaoVendedor> cq = cb.createQuery(ReputacaoVendedor.class);
		Root<ReputacaoVendedor> reputacaoVendedorRoot = cq.from(ReputacaoVendedor.class);
		
		Predicate predicateUsuario = cb.equal(reputacaoVendedorRoot.<Usuario>get("usuarioVendedor"),cb.parameter(Usuario.class, "usuarioVendedor"));
		
		cq.where(predicateUsuario);
		cq.select(reputacaoVendedorRoot);
		
		TypedQuery<ReputacaoVendedor> q = entityManager.createQuery(cq);
		q.setParameter("usuarioVendedor", usuario);
			
		return q.getResultList();
	}

	@Override
	public ReputacaoVendedor obterReputacaoPorProdutoCompradoCompradorVendedor(Produto produto, Usuario usuarioComprador) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ReputacaoVendedor> cq = cb.createQuery(ReputacaoVendedor.class);
		Root<ReputacaoVendedor> reputacaoVendedorRoot = cq.from(ReputacaoVendedor.class);
		
		Predicate predicateProdutoComprado = cb.equal(reputacaoVendedorRoot.<BigInteger>get("idProduto"),cb.parameter(BigInteger.class, "idProduto"));
		Predicate predicateUsuarioVendedor = cb.equal(reputacaoVendedorRoot.<Usuario>get("usuarioVendedor"),cb.parameter(Usuario.class, "usuarioVendedor"));
		Predicate predicateUsuarioComprador = cb.equal(reputacaoVendedorRoot.<Usuario>get("usuarioComprador"),cb.parameter(Usuario.class, "usuarioComprador"));
		
		cq.where(predicateProdutoComprado, predicateUsuarioVendedor, predicateUsuarioComprador);
		cq.select(reputacaoVendedorRoot);
		
		TypedQuery<ReputacaoVendedor> q = entityManager.createQuery(cq);
		q.setParameter("idProduto", produto.getId());
		q.setParameter("usuarioVendedor", produto.getUsuario());
		q.setParameter("usuarioComprador", usuarioComprador);
			
		List<ReputacaoVendedor> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

}
