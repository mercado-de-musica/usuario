package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.TelefoneDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Telefone;
import br.com.mercadodemusica.entities.Usuario;

@Repository
public class TelefoneDaoImpl extends DaoImpl<Telefone> implements TelefoneDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public TelefoneDaoImpl() {
		super(Telefone.class);
	}


	public List<Telefone> obterTelefonesPorUsuario(Usuario usuario) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Telefone> cq = cb.createQuery(Telefone.class);
		Root<Telefone> telefone = cq.from(Telefone.class);
		
		cq.where(cb.equal(telefone.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario")));
		cq.select(telefone);
		TypedQuery<Telefone> q = entityManager.createQuery(cq);
		q.setParameter("usuario", usuario);
				
		return q.getResultList();
	}

	

	
}
