package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.TokenLeadDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.TokenLead;

@Repository
public class TokenLeadDaoImpl extends DaoImpl<TokenLead> implements TokenLeadDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenLeadDaoImpl() {
		super(TokenLead.class);
	}

	@Override
	@Transactional(readOnly = true)
	public TokenLead obterPorToken(String token) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<TokenLead> cq = cb.createQuery(TokenLead.class);
		Root<TokenLead> TokenLead = cq.from(TokenLead.class);
		
		cq.where(cb.like(TokenLead.<String>get("token"),cb.parameter(String.class, "token")));
		
		TypedQuery<TokenLead> q = entityManager.createQuery(cq);
		q.setParameter("token", token);
		
		List<TokenLead> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

}
