package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.TokenLiberacaoUsuarioDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.TokenLiberacaoUsuario;

@Repository
public class TokenLiberacaoUsuarioDaoImpl extends DaoImpl<TokenLiberacaoUsuario> implements TokenLiberacaoUsuarioDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenLiberacaoUsuarioDaoImpl() {
		super(TokenLiberacaoUsuario.class);
	}

	@Override
	public TokenLiberacaoUsuario obterPorToken(String token) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<TokenLiberacaoUsuario> cq = cb.createQuery(TokenLiberacaoUsuario.class);
		Root<TokenLiberacaoUsuario> tokenLiberacaoDeUsuarioRoot = cq.from(TokenLiberacaoUsuario.class);
		
		cq.where(cb.equal(tokenLiberacaoDeUsuarioRoot.<String>get("token"),cb.parameter(String.class, "token")));
		cq.select(tokenLiberacaoDeUsuarioRoot);
		TypedQuery<TokenLiberacaoUsuario> q = entityManager.createQuery(cq);
		q.setParameter("token", token);
		
		List<TokenLiberacaoUsuario> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}
}
