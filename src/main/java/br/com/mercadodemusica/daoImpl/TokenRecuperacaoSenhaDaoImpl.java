package br.com.mercadodemusica.daoImpl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.TokenRecuperacaoSenhaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.TokenRecuperacaoSenha;

@Repository
public class TokenRecuperacaoSenhaDaoImpl extends DaoImpl<TokenRecuperacaoSenha> implements TokenRecuperacaoSenhaDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TokenRecuperacaoSenhaDaoImpl() {
		super(TokenRecuperacaoSenha.class);
	}

	public TokenRecuperacaoSenha trazerDadosViaToken(TokenRecuperacaoSenha tokenRecuperacaoDeSenha) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<TokenRecuperacaoSenha> cq = cb.createQuery(TokenRecuperacaoSenha.class);
		Root<TokenRecuperacaoSenha> tokenRecuperacaoSenhaRoot = cq.from(TokenRecuperacaoSenha.class);
		
		cq.where(cb.equal(tokenRecuperacaoSenhaRoot.<String>get("token"),cb.parameter(String.class, "token")));
		cq.select(tokenRecuperacaoSenhaRoot);
		TypedQuery<TokenRecuperacaoSenha> q = entityManager.createQuery(cq);
		q.setParameter("token", tokenRecuperacaoDeSenha.getToken());
		
		List<TokenRecuperacaoSenha> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
		
		
	}

	public List<TokenRecuperacaoSenha> obterTokensGeradosAposUmaHora(TokenRecuperacaoSenha tokenRecuperacaoDeSenha) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<TokenRecuperacaoSenha> cq = cb.createQuery(TokenRecuperacaoSenha.class);
		Root<TokenRecuperacaoSenha> tokenRecuperacaoSenhaRoot = cq.from(TokenRecuperacaoSenha.class);
		
		Predicate predicateData = cb.lessThanOrEqualTo(tokenRecuperacaoSenhaRoot.<Calendar>get("dataRecuperacaoSenha"), cb.parameter(Calendar.class, "dataRecuperacaoSenha"));
		Predicate predicateAtivo = cb.isTrue(tokenRecuperacaoSenhaRoot.<Boolean>get("ativo"));
		
		cq.where(predicateData, predicateAtivo);

		TypedQuery<TokenRecuperacaoSenha> q = entityManager.createQuery(cq);
		q.setParameter("dataRecuperacaoSenha", tokenRecuperacaoDeSenha.getDataRecuperacaoSenha());
		
		return q.getResultList();
	}
}
