package br.com.mercadodemusica.daoImpl;

import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.UsuarioCrawlerDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.UsuarioCrawler;

@Repository
public class UsuarioCrawlerDaoImpl extends DaoImpl<UsuarioCrawler> implements UsuarioCrawlerDao{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioCrawlerDaoImpl() {
		super(UsuarioCrawler.class);
	}

}
