package br.com.mercadodemusica.daoImpl;

import java.math.BigInteger;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.UsuarioDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Endereco;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.transformacaoDeDados.TracosPontosBarras;

@Repository
public class UsuarioDaoImpl extends DaoImpl<Usuario> implements UsuarioDao {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Logger logger = LogManager.getLogger(UsuarioDaoImpl.class);
	

	public UsuarioDaoImpl() {
		super(Usuario.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BigInteger> obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, Integer quantidadeRegistros, Integer pagina , String nome) throws RegraDeNegocioException {
		logger.info("obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, Integer quantidadeRegistros, Integer pagina , String nome)");
		
		/*
		 * trazer usuarios com produtos a venda
		 */
		String quantidadeRegistrosString = "";
		if(quantidadeRegistros != null) {
			quantidadeRegistrosString = "LIMIT " + quantidadeRegistros + " ";
		}
		
		String paginaString = "";
		if(pagina != null && pagina == 1) {
			paginaString = "OFFSET 0 ";
		} else if(pagina != null && pagina > 1) {
			pagina = pagina.intValue() - 1;
			paginaString = "OFFSET " + pagina * quantidadeRegistros + " ";
		}
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
				
				
		
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO' "
				+ "ORDER BY usuario.id, endereco.posicao <-> ST_Geomfromtext('POINT(" + endereco.getLongitude() + " " + endereco.getLatitude() + ")', 1000) " + quantidadeRegistrosString + paginaString;

				
				SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
						
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BigInteger> obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, Integer quantidadeDeRegistro, Integer pagina, String nome) throws RegraDeNegocioException {
		logger.info("obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, Integer quantidadeDeRegistro, Integer pagina, String nome)");
		
		/*
		 * trazer usuarios com produtos a venda
		 */
		String quantidadeRegistrosString = "";
		if(quantidadeDeRegistro != null) {
			quantidadeRegistrosString = "LIMIT " + quantidadeDeRegistro + " ";
		}
		
		String paginaString = "";
		if(pagina != null && pagina == 1) {
			paginaString = "OFFSET 0 ";
		} else if(pagina != null && pagina > 1) {
			pagina = pagina.intValue() - 1;
			paginaString = "OFFSET " + pagina * quantidadeDeRegistro + " ";
		}
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
				
				
		
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and usuario.id != " + usuario.getId() + " and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and usuario.id != " + usuario.getId() + " and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO' "
				+ "ORDER BY usuario.id, endereco.posicao <-> ST_Geomfromtext('POINT(" + endereco.getLongitude() + " " + endereco.getLatitude() + ")', 1000) " + quantidadeRegistrosString + paginaString;

				
				SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
				
//		LIMIT "+produtosPorPaginaBigInteger+" OFFSET "+comecoDeRegistros
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BigInteger> obterIdsUsuariosComProdutos(Integer quantidadeRegistros, Integer pagina, String nome) throws RegraDeNegocioException {
		logger.info("obterIdsUsuariosComProdutos(Integer quantidadeRegistros, Integer pagina, String nome)");
		
		/*
		 * trazer usuarios com produtos a venda
		 */
		String quantidadeRegistrosString = "";
		if(quantidadeRegistros != null) {
			quantidadeRegistrosString = "LIMIT " + quantidadeRegistros + " ";
		}
		
		String paginaString = "";
		if(pagina != null && pagina == 1) {
			paginaString = "OFFSET 0 ";
		} else if(pagina != null && pagina > 1) {
			pagina = pagina.intValue() - 1;
			paginaString = "OFFSET " + pagina * quantidadeRegistros + " ";
		}
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
				
				
		
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO' "
				+ "ORDER BY usuario.id " + quantidadeRegistrosString + paginaString;

				
				SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
				
//		LIMIT "+produtosPorPaginaBigInteger+" OFFSET "+comecoDeRegistros
		
		return query.list();
	}

	@Override
	public BigInteger obterContagemUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, String nome) throws RegraDeNegocioException {
		logger.info("obterContagemUsuariosPorProximidadeDeEnderecoComProdutos(Endereco endereco, String nome)");
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
				
				
		
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO'";

		SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
						
		return BigInteger.valueOf(query.list().size());
	}

	@Override
	public BigInteger obterContagemUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, String nome) throws RegraDeNegocioException {
		logger.info("obterContagemUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(Usuario usuario, Endereco endereco, String nome)");
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
				
				
		
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and usuario.id != " + usuario.getId() + " and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and usuario.id != " + usuario.getId() + " and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO'";

				
				SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
				
		
		return BigInteger.valueOf(query.list().size());
	}

	@Override
	public BigInteger obterContagemUsuariosComProdutos(String nome) throws RegraDeNegocioException {
		logger.info("obterContagemUsuariosComProdutos(String nome)");
		
		Session session = this.entityManager.unwrap(Session.class);
		String queryString = "SELECT DISTINCT ON (usuario.id) usuario.id, usuario_pessoa_fisica.id, usuario_pessoa_juridica.id, 1 as clazz_ FROM usuario usuario "
				+ "left join usuario_pessoa_fisica usuario_pessoa_fisica on usuario.id = usuario_pessoa_fisica.id "
				+ "left join usuario_pessoa_juridica usuario_pessoa_juridica on usuario.id = usuario_pessoa_juridica.id "
				+ "inner join infos_gerais_produto_usuario infosGeraisUsuario on usuario.id = infosGeraisUsuario.id_usuario "
				+ "inner join endereco endereco on endereco.id_usuario = usuario.id "
				+ "inner join apresentacao_infos_gerais_produto apresentacao_infos_gerais_produto on apresentacao_infos_gerais_produto.id_infos_gerais_produto_usuario = infosGeraisUsuario.id "
				+ "left join compra compra on infosGeraisUsuario.id_compra = compra.id "
				+ "left join historico_datas_de_compra historicoDatasDeCompra on historicoDatasDeCompra.id_compra = compra.id ";
				
		if(nome != null) {
			TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(nome);
			nome = tracosPontosBarras.getString();
			
			queryString = queryString + "where  usuario.ativo is true and (LOWER(usuario_pessoa_fisica.nome) like LOWER('%" + nome + "%') OR "
					+ "LOWER(usuario_pessoa_fisica.sobrenome) like LOWER('%" + nome + "%') OR LOWER(usuario_pessoa_juridica.nome_fantasia) like LOWER('%" + nome + "%') "
					+ "OR LOWER(usuario_pessoa_juridica.razao_social) like LOWER('%" + nome + "%')) "
					+ "and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		} else {
			queryString = queryString + "where usuario.ativo is true and (compra is null or (compra.finalizado = false and date(historicoDatasDeCompra.data_da_compra) != (SELECT current_date) ";
		}
		
		queryString = queryString + "and historicoDatasDeCompra.data_atualizada = true)) and apresentacao_infos_gerais_produto.tipo_apresentacao = 'FOTO' ";
		SQLQuery query = session.createSQLQuery(queryString).addScalar("id", StandardBasicTypes.BIG_INTEGER);		
		
		return BigInteger.valueOf(query.list().size());
	}
}
