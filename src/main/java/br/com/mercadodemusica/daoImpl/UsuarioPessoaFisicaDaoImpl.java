package br.com.mercadodemusica.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.UsuarioPessoaFisicaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.entities.UsuarioPessoaFisica;

@Repository
public class UsuarioPessoaFisicaDaoImpl extends DaoImpl<UsuarioPessoaFisica> implements UsuarioPessoaFisicaDao {

	public UsuarioPessoaFisicaDaoImpl() {
		super(UsuarioPessoaFisica.class);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioPessoaFisica selectPorCpf(String cpf) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UsuarioPessoaFisica> cq = cb.createQuery(UsuarioPessoaFisica.class);
		Root<UsuarioPessoaFisica> usuarioPessoaFisicaRoot = cq.from(UsuarioPessoaFisica.class);
		
		cq.where(cb.equal(usuarioPessoaFisicaRoot.<String>get("cpf"),cb.parameter(String.class, "cpf")));
		cq.select(usuarioPessoaFisicaRoot);
		TypedQuery<UsuarioPessoaFisica> q = entityManager.createQuery(cq);
		q.setParameter("cpf", cpf);
		
		List<UsuarioPessoaFisica> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	public UsuarioPessoaFisica selectPorUsuario(Usuario usuario) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UsuarioPessoaFisica> cq = cb.createQuery(UsuarioPessoaFisica.class);
		Root<UsuarioPessoaFisica> usuarioPessoaFisicaRoot = cq.from(UsuarioPessoaFisica.class);
		
		cq.where(cb.equal(usuarioPessoaFisicaRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario")));
		cq.select(usuarioPessoaFisicaRoot);
		TypedQuery<UsuarioPessoaFisica> q = entityManager.createQuery(cq);
		q.setParameter("usuario", usuario);
		
		List<UsuarioPessoaFisica> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}
}
