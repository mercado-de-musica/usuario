package br.com.mercadodemusica.daoImpl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.mercadodemusica.dao.UsuarioPessoaJuridicaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.entities.UsuarioPessoaJuridica;

@Repository
public class UsuarioPessoaJuridicaDaoImpl extends DaoImpl<UsuarioPessoaJuridica> implements UsuarioPessoaJuridicaDao {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Logger logger = LogManager.getLogger(UsuarioPessoaJuridicaDaoImpl.class);
	
	
	public UsuarioPessoaJuridicaDaoImpl() {
		super(UsuarioPessoaJuridica.class);
	}
	
	
	public UsuarioPessoaJuridica selectPorCnpj(String cnpj) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UsuarioPessoaJuridica> cq = cb.createQuery(UsuarioPessoaJuridica.class);
		Root<UsuarioPessoaJuridica> usuarioPessoaJuridicaRoot = cq.from(UsuarioPessoaJuridica.class);
		
		cq.where(cb.equal(usuarioPessoaJuridicaRoot.<String>get("cnpj"),cb.parameter(String.class, "cnpj")));
		cq.select(usuarioPessoaJuridicaRoot);
		TypedQuery<UsuarioPessoaJuridica> q = entityManager.createQuery(cq);
		q.setParameter("cnpj", cnpj);
		
		List<UsuarioPessoaJuridica> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}
	
	@Override
	public UsuarioPessoaJuridica selectPorCpfTitular(String cpfTitular) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UsuarioPessoaJuridica> cq = cb.createQuery(UsuarioPessoaJuridica.class);
		Root<UsuarioPessoaJuridica> usuarioPessoaJuridicaRoot = cq.from(UsuarioPessoaJuridica.class);
		
		cq.where(cb.equal(usuarioPessoaJuridicaRoot.<String>get("cpfTitular"),cb.parameter(String.class, "cpfTitular")));
		cq.select(usuarioPessoaJuridicaRoot);
		TypedQuery<UsuarioPessoaJuridica> q = entityManager.createQuery(cq);
		q.setParameter("cpfTitular", cpfTitular);
		
		List<UsuarioPessoaJuridica> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	public UsuarioPessoaJuridica selectPorUsuario(Usuario usuario) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<UsuarioPessoaJuridica> cq = cb.createQuery(UsuarioPessoaJuridica.class);
		Root<UsuarioPessoaJuridica> usuarioPessoaJuridicaRoot = cq.from(UsuarioPessoaJuridica.class);
		
		cq.where(cb.equal(usuarioPessoaJuridicaRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario")));
		cq.select(usuarioPessoaJuridicaRoot);
		TypedQuery<UsuarioPessoaJuridica> q = entityManager.createQuery(cq);
		q.setParameter("usuario", usuario);
		
		List<UsuarioPessoaJuridica> lista = q.getResultList();
		
		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
	}

	@Override
	public UsuarioPessoaJuridica obterUsuarioPessoaJuridicaPorId(BigInteger id) {
		
		
		logger.info("obterUsuarioPessoaJuridicaPorId(BigInteger id)");
		
		Session session = this.entityManager.unwrap(Session.class);
		
		Query query = session.createSQLQuery("select usuarioPessoaJuridica.* from usuario_pessoa_juridica as usuarioPessoaJuridica where usuarioPessoaJuridica.id = :id")
				.addEntity(UsuarioPessoaJuridica.class)
				.setParameter("id", id.intValue());
				
		return (UsuarioPessoaJuridica) query.uniqueResult();
	}
}
