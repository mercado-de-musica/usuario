package br.com.mercadodemusica.restController;

import java.math.BigInteger;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.AcessoUsuarioService;

@RestController
@RequestMapping(value = "/acessoUsuario")
public class AcessoUsuarioRestController {
	
	@Autowired
	private AcessoUsuarioService acessoUsuarioService;
	
	@RequestMapping(value = "/obterPorUsuarioId/{idUsuario}")
	public AcessoUsuarioDTO obterPorUsuarioId(@PathVariable("idUsuario") BigInteger idUsuario) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return acessoUsuarioService.obterPorUsuarioId(idUsuario);
	}
	
	@RequestMapping(value = "/verificarEmail/{email:.+}")
	public Boolean verificarEmail(@PathVariable("email") String email) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return !acessoUsuarioService.verificarEmail(email);
	}
}
