package br.com.mercadodemusica.restController;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.service.EnderecoService;

@RestController
@RequestMapping(value = "/endereco")
public class EnderedoRestController {
	
	@Autowired
	private EnderecoService enderecoService;
	
	
	@RequestMapping(value = "/obterEnderecoPorCep/{cep}")
	public String obterEnderecoPorCep (@PathVariable("cep") String cep) throws MalformedURLException, IOException, JSONException {
		
		return enderecoService.obterEnderecoPorCep(cep);
	
	}
}
