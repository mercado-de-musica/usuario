package br.com.mercadodemusica.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.LeadService;

@RestController
@RequestMapping("/lead")
public class LeadRestController {
	
	@Autowired
	private LeadService leadService;
	
	@RequestMapping(value = "/inserirLead", method = RequestMethod.POST)
	public void inserirLead (@RequestBody LeadDTO leadDto) throws RegraDeNegocioException{
		
		leadService.inserirLead(leadDto);
	}
	
	@RequestMapping(value = "/obterLeadPorEmail/{email:.+}")
	public LeadDTO obterLeadPorEmail (@PathVariable("email") String email) throws RegraDeNegocioException{
		
		return leadService.obterLeadPorEmail(email);
	}
	
}
