package br.com.mercadodemusica.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.TokenLeadService;

@RestController
@RequestMapping("/tokenLead")
public class TokenLeadRestController {

	
	@Autowired
	private TokenLeadService tokenLeadService;
	
	@RequestMapping(value = "/criarTokenLead", method = RequestMethod.POST)
	public TokenLeadDTO criarTokenLead(@RequestBody LeadDTO leadDto) throws EmailException, RegraDeNegocioException {
		return tokenLeadService.criarTokenLead(leadDto);		
	}
	
	@RequestMapping(value = "/confirmarLeadFacade", method = RequestMethod.POST)
	public void confirmarLeadFacade(@RequestBody String token) throws EmailException, RegraDeNegocioException {
		tokenLeadService.confirmarLeadFacade(token);		
	}
	
	@RequestMapping(value = "/obterPorToken/{token}")
	public TokenLeadDTO obterPorToken(@PathVariable("token") String token) throws EmailException, RegraDeNegocioException {
		return tokenLeadService.obterPorToken(token);		
	}
}
