package br.com.mercadodemusica.restController;

import java.math.BigInteger;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.TokenLiberacaoUsuarioService;

@RestController
@RequestMapping(value = "/tokenLiberacaoUsuario")
public class TokenLiberacaoUsuarioRestController {

	@Autowired
	private TokenLiberacaoUsuarioService tokenLiberacaoUsuarioService;
	
	
	@RequestMapping(value = "/criarTokenDeLiberacaoPorIdUsuario/{idUsuario}")
	public TokenLiberacaoUsuarioDTO criarTokenDeLiberacaoPorIdUsuario(@PathVariable("idUsuario") BigInteger idUsuario) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return tokenLiberacaoUsuarioService.criarTokenDeLiberacao(idUsuario);
	}
	
	@RequestMapping(value = "/obterPorToken/{token}")
	public TokenLiberacaoUsuarioDTO obterPorToken(@PathVariable("token") String token) throws TokenException {
		return tokenLiberacaoUsuarioService.obterPorToken(token);
	}
	
	@RequestMapping(value = "/inativarToken")
	public void inativarToken(@RequestBody TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto) throws TokenException {
		tokenLiberacaoUsuarioService.inativarToken(tokenLiberacaoUsuarioDto);
	}
}
