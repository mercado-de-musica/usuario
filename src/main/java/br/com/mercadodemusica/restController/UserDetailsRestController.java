package br.com.mercadodemusica.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/userDetails")
public class UserDetailsRestController {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@RequestMapping(value = "/loadUserByUsername")
	public UserDetails loadUserByUsername(@RequestParam("username") String username) {
		return userDetailsService.loadUserByUsername(username);
	}
	
}
