package br.com.mercadodemusica.restController;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mercadodemusica.dto.EnderecoDTO;
import br.com.mercadodemusica.dto.TelefoneDTO;
import br.com.mercadodemusica.dto.UsuarioCrawlerDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.exceptions.EnderecoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.UsuarioService;

@RestController
@CrossOrigin
@RequestMapping("/usuario")
public class UsuarioRestController {
	
	@Autowired
	private UsuarioService usuarioService;
	
//	@RequestMapping(value = "/")
//	public JSONObject index() throws JSONException {
//		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("teste", "ok");
//		
//		return jsonObject;
//	}
	
	
	@RequestMapping(value = "/cadastrarUsuarioCrawler", method = RequestMethod.POST)
	public void cadastrarUsuarioPessoaFisicaOuJuridica(@RequestBody UsuarioCrawlerDTO usuarioDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception {
		usuarioService.cadastrarUsuarioPessoaFisicaOuJuridica(usuarioDto);	
	}
	
	
	@RequestMapping(value = "/cadastrar_endereco_usuario", method = RequestMethod.POST)
	public void cadastrarEnderecoUsuario (@RequestBody EnderecoDTO enderecoDTO) throws RegraDeNegocioException{
		UsuarioDTO usuarioDto = new UsuarioPessoaFisicaDTO();
		usuarioDto.setId(enderecoDTO.getIdUsuario());
		UsuarioDTO usuarioRetornoDto = usuarioService.obterInfosUsuarioPorUsuario(usuarioDto);
		enderecoDTO.setUsuario(usuarioRetornoDto);
		
		ArrayList<EnderecoDTO> listaDeEnderecoDto = new ArrayList<EnderecoDTO>();
		listaDeEnderecoDto.add(enderecoDTO);
		
		usuarioService.validarEndereco(listaDeEnderecoDto);
		usuarioService.atualizarEnderecos(listaDeEnderecoDto);
	}
	
	@RequestMapping(value = "/cadastrar_telefone_usuario", method = RequestMethod.POST)
	public void cadastrarEnderecoUsuario (@RequestBody TelefoneDTO telefoneDto) throws RegraDeNegocioException{
		UsuarioDTO usuarioDto = new UsuarioPessoaFisicaDTO();
		usuarioDto.setId(telefoneDto.getIdUsuario());
		UsuarioDTO usuarioRetornoDto = usuarioService.obterInfosUsuarioPorUsuario(usuarioDto);
		telefoneDto.setUsuario(usuarioRetornoDto);
		
		ArrayList<TelefoneDTO> listaDeTelefonesDto = new ArrayList<TelefoneDTO>();
		listaDeTelefonesDto.add(telefoneDto);
		
		usuarioService.validarTelefones(listaDeTelefonesDto);
		usuarioService.atualizarTelefones(listaDeTelefonesDto);
	}
	
	@RequestMapping(value = "/obterUsuarioEnderecoPesquisaProdutos")
	public UsuarioDTO obterUsuarioEnderecoPesquisaProdutos(@RequestParam("jsonObject") JSONObject jsonObject) throws JsonParseException, JsonMappingException, UsuarioException, JSONException, IOException, EnderecoException, com.vividsolutions.jts.io.ParseException  {
		UsuarioDTO usuario = usuarioService.obterUsuarioEnderecoPesquisaProdutos(jsonObject);
		return usuario;
	}
	
	@RequestMapping(value = "/cadastrarUsuarioPessoaFisica", method = RequestMethod.POST)
	public void cadastrarUsuarioPessoaFisica (@RequestBody UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception {
		usuarioService.cadastrarUsuarioPessoaFisica(usuarioPessoaFisicaDto);
	}
	
	@RequestMapping(value = "/cadastrarUsuarioPessoaJuridica", method = RequestMethod.POST)
	public void cadastrarUsuarioPessoaJuridica (@RequestBody UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception {
		usuarioService.cadastrarUsuarioPessoaJuridica(usuarioPessoaJuridicaDto);
	}
	
	
	@RequestMapping(value = "/verificarCpfNaoExistente/{cpf:.+}")
	public Boolean verificarCpfNaoExistente(@PathVariable("cpf") String cpf) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return usuarioService.verificarCpfNaoExistente(cpf);
	}
	
	
	@RequestMapping(value = "/verificarCnpjNaoExistente/{cnpj:.+}")
	public Boolean verificarCnpjNaoExistente(@PathVariable("cnpj") String cnpj) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return usuarioService.verificarCnpjNaoExistente(cnpj);
	}
	
	
	@RequestMapping(value = "/verificarCpfPessoaJuridicaNaoExistente/{cpfTitular:.+}")
	public Boolean verificarCpfPessoaJuridicaNaoExistente(@PathVariable("cpfTitular") String cpfTitular) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return usuarioService.verificarCpfPessoaJuridicaNaoExistente(cpfTitular);
	}
	
	
	@RequestMapping(value = "/obterInfosUsuarioPorCpfCnpj/{cpfCnpj:.+}")
	public UsuarioDTO obterInfosUsuarioPorCpfCnpj(@PathVariable("cpfCnpj") String cpfCnpj) throws UsuarioException, DocumentException, RegraDeNegocioException {
		UsuarioDTO usuarioDto = usuarioService.obterInfosUsuarioPorCpfCnpj(cpfCnpj);
		return usuarioDto;
	}
	
	
	@RequestMapping(value = "/verificarDataDeNascimento/{dataDeNascimento:.+}")
	public Boolean verificarDataDeNascimento(@PathVariable("dataDeNascimento") String dataDeNascimento) throws UsuarioException, DocumentException, RegraDeNegocioException {
		return usuarioService.verificarDataDeNascimento(dataDeNascimento);
	}
	
	
	@RequestMapping(value = "/liberarUsuario", method = RequestMethod.POST)
	public void liberarUsuario(@RequestBody UsuarioDTO usuarioDto) throws TokenException {
		usuarioService.liberarUsuario(usuarioDto);
	}
	
}
