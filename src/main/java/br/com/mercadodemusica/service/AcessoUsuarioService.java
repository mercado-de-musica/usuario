package br.com.mercadodemusica.service;

import java.math.BigInteger;

import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;

public interface AcessoUsuarioService {

	AcessoUsuarioDTO obterPorUsuarioId(BigInteger idUsuario) throws RegraDeNegocioException;

	Boolean verificarEmail(String email) throws RegraDeNegocioException;

	void inserirAcessoUsuario(AcessoUsuarioDTO acessoUsuarioDto);

}
