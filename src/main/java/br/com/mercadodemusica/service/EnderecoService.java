package br.com.mercadodemusica.service;

import java.io.IOException;

import org.json.JSONException;

public interface EnderecoService {
	
	String obterEnderecoPorCep(String cep) throws IOException, JSONException;
	
}
