package br.com.mercadodemusica.service;

import br.com.mercadodemusica.dto.InteresseLeadDTO;

public interface InteressesLeadService {

	void inserirInteresseLead(InteresseLeadDTO interesseLeadDto);

}
