package br.com.mercadodemusica.service;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.UsuarioException;

public interface LeadService {

	void inserirLead(LeadDTO leadDto) throws UsuarioException, EmailException, RegraDeNegocioException;

	LeadDTO obterLeadPorEmail(String email);

}
