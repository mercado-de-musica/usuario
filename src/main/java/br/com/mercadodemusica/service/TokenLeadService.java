package br.com.mercadodemusica.service;

import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;

public interface TokenLeadService {

	TokenLeadDTO criarTokenLead(LeadDTO leadDto) throws EmailException, RegraDeNegocioException;

	void confirmarLeadFacade(String token) throws RegraDeNegocioException;

	TokenLeadDTO obterPorToken(String token) throws RegraDeNegocioException;

}
