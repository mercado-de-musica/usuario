package br.com.mercadodemusica.service;

import java.io.Serializable;
import java.math.BigInteger;

import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.TokenException;

public interface TokenLiberacaoUsuarioService extends Serializable {

	TokenLiberacaoUsuarioDTO criarTokenDeLiberacao(BigInteger idUsuario) throws RegraDeNegocioException;

	TokenLiberacaoUsuarioDTO obterPorToken(String token) throws TokenException;

	void inativarToken(TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto);

}
