package br.com.mercadodemusica.service;

import java.io.Serializable;

import br.com.mercadodemusica.dto.TokenRecuperacaoSenhaDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;

public interface TokenService extends Serializable {
	
	TokenRecuperacaoSenhaDTO tokenRecuperacaoUsuario(UsuarioDTO usuarioDto) throws UsuarioException;

	TokenRecuperacaoSenhaDTO trazerDadosViaToken(String token) throws TokenException;

	void cancelarToken(TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDto);

	void inativarTokensPorHora();
	
}
