package br.com.mercadodemusica.service;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.DocumentException;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mercadodemusica.dto.EnderecoDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.ReputacaoVendedorDTO;
import br.com.mercadodemusica.dto.TelefoneDTO;
import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.exceptions.DocumentosException;
import br.com.mercadodemusica.exceptions.EnderecoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.ReputacaoVendedorException;
import br.com.mercadodemusica.exceptions.TelefoneException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;


public interface UsuarioService extends Serializable {
	
	void cadastrarUsuarioPessoaFisicaOuJuridica(UsuarioDTO usuarioDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception;
	
	void cadastrarUsuarioPessoaFisica(UsuarioPessoaFisicaDTO usuarioDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception;
	
	void cadastrarUsuarioPessoaJuridica(UsuarioPessoaJuridicaDTO usuarioDto) throws RegraDeNegocioException, IOException, InterruptedException, ParseException, Exception;

	Boolean verificarCpfNaoExistente(String cpf) throws RegraDeNegocioException;

	Boolean verificarCnpjNaoExistente(String cnpj) throws RegraDeNegocioException;
	
	Boolean verificarCpfPessoaJuridicaNaoExistente(String cpfTitular) throws RegraDeNegocioException;
	
	UsuarioDTO obterInfosUsuario(String remoteUser) throws UsuarioException;
	
	UsuarioDTO obterInfosUsuarioPorCpfCnpj(String cpfCnpj) throws DocumentException, UsuarioException, RegraDeNegocioException;

	UsuarioDTO obterUsuarioEnderecoPesquisaProdutos(JSONObject jsonObject) throws UsuarioException, JSONException, JsonParseException, JsonMappingException, IOException, EnderecoException, com.vividsolutions.jts.io.ParseException ;
	
	UsuarioDTO obterInfosUsuarioPorUsuario(UsuarioDTO usuarioDto);

	void cadastrarReputacaoDoVendedor(ReputacaoVendedorDTO reputacaoVendedorDto) throws ReputacaoVendedorException;

	String verificarReputacaoVendedor(ProdutoDTO produtoDTO);

	Boolean verificarDataDeNascimento(String dataDeNascimentoString);

	void obterInfosUsuarioVerificacaoOnLine(HttpServletRequest request) throws UsuarioException;

	void verificacaoCpfCnpj(String cpfCnpj) throws DocumentosException;

	void transformacaoDeData(UsuarioDTO usuarioDto);

	void liberarUsuario(UsuarioDTO usuarioDto) throws TokenException;

//	List<ReputacaoVendedorDTO> obterReputacaoVendedorProduto(List<CompraDTO> listaDeComprasFinalizadas);

	UsuarioDTO obterInfosUsuarioPorId(BigInteger idUsuario);

//	String obterNomeDaLojaOnline(UsuarioDTO usuarioDonoDaLojaDto, List<InstrumentoAcessorioLojaOnlineDTO> listaInstrumentoAcessorioLojaOnlineDto);

//	List<LojaOnlineDTO> obterLojasOnline(UsuarioDTO usuarioDto) throws EnderecoException;
	
	List<UsuarioDTO> obterUsuariosPorProximidadeComProdutos(UsuarioDTO usuarioDto, Integer quantidadeRegistros) throws EnderecoException, RegraDeNegocioException;

	List<UsuarioDTO> obterLojasOnlinePorNome(UsuarioDTO usuarioDto, String nome) throws EnderecoException, RegraDeNegocioException;

	Map<String, Object> obterListaDeLojasOnLine(UsuarioDTO usuarioDto, BigInteger quantidadeRegistros, BigInteger pagina) throws EnderecoException, RegraDeNegocioException;
	
	void validarEndereco(List<EnderecoDTO> enderecos) throws EnderecoException, RegraDeNegocioException;
	
	void atualizarEnderecos(List<EnderecoDTO> listaDeEnderecos) throws EnderecoException;
	
	void validarTelefones(List<TelefoneDTO> telefonesList) throws TelefoneException, RegraDeNegocioException;
	
	void atualizarTelefones(List<TelefoneDTO> listaDeTelefones);
}
