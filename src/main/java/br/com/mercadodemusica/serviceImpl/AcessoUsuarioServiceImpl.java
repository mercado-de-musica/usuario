package br.com.mercadodemusica.serviceImpl;

import java.math.BigInteger;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.dao.UsuarioDao;
import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.AcessoUsuarioService;

@Service
public class AcessoUsuarioServiceImpl implements AcessoUsuarioService {

	@Autowired
	private AcessoUsuarioDao acessoUsuarioDao;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private Mapper dozerMapper;
	
	@Override
	public AcessoUsuarioDTO obterPorUsuarioId(BigInteger idUsuario) throws RegraDeNegocioException {
		
		if(idUsuario == null) {
			throw new RegraDeNegocioException("O id náo pode estar vazio");
		}
		
		Usuario usuario = usuarioDao.selectOne(idUsuario);
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorUsuario(usuario);
		
		AcessoUsuarioDTO acessoUsuarioDto = dozerMapper.map(acessoUsuario, AcessoUsuarioDTO.class);
		
		return acessoUsuarioDto;
	}

	@Override
	public Boolean verificarEmail(String email) throws RegraDeNegocioException {
//		Apenas validação de email
		
		if(email == null || email.isEmpty()) {
			throw new RegraDeNegocioException("O e-mail não pode estar vazio");
		}
	
		
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorEmail(email);
	
		Boolean retorno = true;
		if(acessoUsuario == null) {
			retorno = false;
		}
		return retorno;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void inserirAcessoUsuario(AcessoUsuarioDTO acessoUsuarioDto) {
		AcessoUsuario acessoUsuario = dozerMapper.map(acessoUsuarioDto, AcessoUsuario.class);
		acessoUsuarioDao.save(acessoUsuario);
	}
	
	
	
	
}
