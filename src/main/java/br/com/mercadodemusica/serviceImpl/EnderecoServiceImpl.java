package br.com.mercadodemusica.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import br.com.mercadodemusica.service.EnderecoService;

@Service
public class EnderecoServiceImpl implements EnderecoService {

	public String obterEnderecoPorCep(String cep) throws IOException, JSONException {
		String retorno = null;
		
		URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep="+cep+"&formato=json");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();            
		connection.addRequestProperty("Request-Method","GET");      
		connection.setRequestProperty("accept", "application/json");
		  
		connection.setDoInput(true);    
		connection.setDoOutput(false);    
		connection.connect();    
		       
		if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {    
		    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));    
		    String newData = "";    
		    String s = "";    
		    while (null != ((s = br.readLine()))) {    
		        newData += s;    
		    }    
		    br.close();    
		    
		    retorno = new JSONObject(newData).toString();
		} 
	    
		return retorno;
	}
	
}
