package br.com.mercadodemusica.serviceImpl;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.InteresseLeadDao;
import br.com.mercadodemusica.dto.InteresseLeadDTO;
import br.com.mercadodemusica.entities.InteresseLead;
import br.com.mercadodemusica.service.InteressesLeadService;

@Service
public class InteressesLeadServiceImpl implements InteressesLeadService {

	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private InteresseLeadDao interesseLeadDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void inserirInteresseLead(InteresseLeadDTO interesseLeadDto) {
		InteresseLead interesseLead = dozerBeanMapper.map(interesseLeadDto, InteresseLead.class);
		List<InteresseLead> listaDeInteressesLead = interesseLeadDao.obterPorNomeAndLead(interesseLead);
		
		if(listaDeInteressesLead.isEmpty()) {
			interesseLeadDao.save(interesseLead);
		} else {
			for(InteresseLead interesseLeadDatabase : listaDeInteressesLead) {
				if(!interesseLeadDatabase.getNome().toLowerCase().equals(interesseLead.getNome().toLowerCase())) {
					interesseLeadDao.save(interesseLead);			
				}
			}
		}	
	}
}
