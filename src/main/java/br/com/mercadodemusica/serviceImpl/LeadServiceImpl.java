package br.com.mercadodemusica.serviceImpl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.LeadDao;
import br.com.mercadodemusica.dto.InteresseLeadDTO;
import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.entities.Lead;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.InteressesLeadService;
import br.com.mercadodemusica.service.LeadService;
import br.com.mercadodemusica.transformacaoDeDados.Email;
import br.com.mercadodemusica.transformacaoDeDados.TracosPontosBarras;

@Service
public class LeadServiceImpl implements LeadService {
	
	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private LeadDao leadDao;
	
	@Autowired
	private InteressesLeadService interessesLeadService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void inserirLead(LeadDTO leadDto) throws RegraDeNegocioException {
	
		this.verificarNome(leadDto.getNome());
		this.verificarEmail(leadDto.getEmail());
		
		TracosPontosBarras tracosPontosBarras = new TracosPontosBarras(leadDto.getNome());
		leadDto.setNome(tracosPontosBarras.getString());
		
		
		List<InteresseLeadDTO> listaDeInteressesDto = leadDto.getInteresses();
		
		leadDto.setAtivo(Boolean.FALSE);
		
		Lead lead = dozerBeanMapper.map(leadDto, Lead.class);
		Lead leadRetorno = leadDao.obterPorEmail(lead.getEmail());
		
		if(leadRetorno != null) {
			lead.setId(leadRetorno.getId());
			leadDao.merge(lead);
		} else {
			lead = leadDao.save(lead);
		}
		
		for(InteresseLeadDTO interesseLeadDto : listaDeInteressesDto) {
			LeadDTO leadDtoComId = dozerBeanMapper.map(lead, LeadDTO.class);
			interesseLeadDto.setLead(leadDtoComId);

			interessesLeadService.inserirInteresseLead(interesseLeadDto);
		}
		
	}

	private void verificarNome(String nome) throws UsuarioException {
		if(nome == null || nome.isEmpty()) {
			throw new UsuarioException("O nome está vazio");
		}	
	}

	private void verificarEmail(String email) throws EmailException {
		
		if(email == null || email.isEmpty()) {
			throw new EmailException("O e-mail está vazio");
		}
		
		
		Pattern pattern = Pattern.compile(Email.getPattern());  
		Matcher matcher = pattern.matcher(email);  
		Boolean matchFound = matcher.matches();  
		if(!matchFound) {
			throw new EmailException("O e-mail está escrito de forma incorreta");
		}
		
	}

	@Override
	public LeadDTO obterLeadPorEmail(String email) {
		Lead lead = leadDao.obterPorEmail(email);
		return dozerBeanMapper.map(lead, LeadDTO.class);
	}
}
