package br.com.mercadodemusica.serviceImpl;

import java.util.Calendar;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.TokenLeadDao;
import br.com.mercadodemusica.dto.LeadDTO;
import br.com.mercadodemusica.dto.TokenLeadDTO;
import br.com.mercadodemusica.entities.TokenLead;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.TokenLeadService;
import br.com.mercadodemusica.transformacaoDeDados.Senha;

@Service
public class TokenLeadServiceImpl implements TokenLeadService {

	@Autowired
	private Mapper dozerMapper;
	
	@Autowired
	private TokenLeadDao tokenLeadDao;
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public TokenLeadDTO criarTokenLead(LeadDTO leadDto) throws RegraDeNegocioException {
		
		Calendar dataAtual = Calendar.getInstance();
		String tokenLeadString = leadDto.getId() + leadDto.getEmail() + dataAtual.getTimeInMillis();
		
		Senha token = new Senha();
		String retornoToken = token.criptografar(tokenLeadString);
		
		TokenLeadDTO tokeanLeadDto = new TokenLeadDTO();
		
		tokeanLeadDto.setLead(leadDto);
		tokeanLeadDto.setToken(retornoToken);
		
		TokenLead tokenLead = dozerMapper.map(tokeanLeadDto, TokenLead.class);
		tokenLeadDao.merge(tokenLead);
		
		return tokeanLeadDto;
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void confirmarLeadFacade(String token) throws RegraDeNegocioException {
		if(token == null || token.isEmpty() ) {
			throw new RegraDeNegocioException("O token enviado está vazio");
		}
		
		TokenLead tokenLead = tokenLeadDao.obterPorToken(token);
		tokenLead.getLead().setAtivo(Boolean.TRUE);
		
		tokenLeadDao.merge(tokenLead);
	}


	@Override
	public TokenLeadDTO obterPorToken(String token) throws RegraDeNegocioException {
		if(token == null || token.isEmpty() ) {
			throw new RegraDeNegocioException("O token enviado está vazio");
		}
		
		TokenLead tokenLead = tokenLeadDao.obterPorToken(token);
		
		if(tokenLead == null ) {
			throw new RegraDeNegocioException("O token está errado");
		}
		
		return dozerMapper.map(tokenLead, TokenLeadDTO.class);
	}

}
