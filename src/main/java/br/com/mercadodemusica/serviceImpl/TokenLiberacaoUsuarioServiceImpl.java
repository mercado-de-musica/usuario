package br.com.mercadodemusica.serviceImpl;

import java.math.BigInteger;
import java.util.Calendar;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.dao.TokenLiberacaoUsuarioDao;
import br.com.mercadodemusica.dao.UsuarioDao;
import br.com.mercadodemusica.dto.TokenLiberacaoUsuarioDTO;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.TokenLiberacaoUsuario;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.service.TokenLiberacaoUsuarioService;
import br.com.mercadodemusica.transformacaoDeDados.Senha;

@Service
public class TokenLiberacaoUsuarioServiceImpl implements TokenLiberacaoUsuarioService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private TokenLiberacaoUsuarioDao tokenLiberacaoUsuarioDao;
	
	@Autowired
	private AcessoUsuarioDao acessoUsuarioDao;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public TokenLiberacaoUsuarioDTO criarTokenDeLiberacao(BigInteger idUsuario) throws RegraDeNegocioException {
		
		if(idUsuario == null ) {
			throw new RegraDeNegocioException("O id náo pode estar vazio");
		}
		
		
		Usuario usuario = usuarioDao.selectOne(idUsuario);
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorUsuario(usuario);
		
		Calendar dataAtual = Calendar.getInstance();
		String tokenDadosUsuario = usuario.getId() + acessoUsuario.getEmail() + acessoUsuario.getSenha() + dataAtual.getTimeInMillis();
		
		Senha token = new Senha();
		String retornoToken = token.criptografar(tokenDadosUsuario);
		
		TokenLiberacaoUsuario tokenRecuperacaoDeSenha = new TokenLiberacaoUsuario();
		
		
		tokenRecuperacaoDeSenha.setAtivo(Boolean.TRUE);
		tokenRecuperacaoDeSenha.setUsuario(usuario);
		tokenRecuperacaoDeSenha.setToken(retornoToken);
		
		tokenLiberacaoUsuarioDao.save(tokenRecuperacaoDeSenha);
		
		return dozerBeanMapper.map(tokenRecuperacaoDeSenha, TokenLiberacaoUsuarioDTO.class);	
	}

	@Override
	@Transactional(readOnly = true)
	public TokenLiberacaoUsuarioDTO obterPorToken(String token) throws TokenException {
		TokenLiberacaoUsuario tokenLiberacaoUsuario = tokenLiberacaoUsuarioDao.obterPorToken(token);
		
		if(tokenLiberacaoUsuario == null) {
			throw new TokenException("Este Token não existe");
		}
		return dozerBeanMapper.map(tokenLiberacaoUsuario, TokenLiberacaoUsuarioDTO.class);	
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void inativarToken(TokenLiberacaoUsuarioDTO tokenLiberacaoUsuarioDto) {
		
		tokenLiberacaoUsuarioDto.setAtivo(Boolean.FALSE);
		TokenLiberacaoUsuario tokenLiberacaoUsuario = dozerBeanMapper.map(tokenLiberacaoUsuarioDto, TokenLiberacaoUsuario.class);	
		
		tokenLiberacaoUsuarioDao.update(tokenLiberacaoUsuario);
		tokenLiberacaoUsuarioDao.flush();
	}
	
	
}
