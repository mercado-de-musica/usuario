package br.com.mercadodemusica.serviceImpl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.dao.TokenRecuperacaoSenhaDao;
import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.dto.TokenRecuperacaoSenhaDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.TokenRecuperacaoSenha;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.TokenService;
import br.com.mercadodemusica.transformacaoDeDados.Senha;


@Service
public class TokenServiceImpl implements TokenService, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private TokenRecuperacaoSenhaDao tokenRecuperacaoSenhaDao;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private AcessoUsuarioDao acessoUsuarioDao;

	@Transactional(propagation = Propagation.REQUIRED)
	public TokenRecuperacaoSenhaDTO tokenRecuperacaoUsuario(UsuarioDTO usuarioDto) throws UsuarioException {
		
//		concatenacao de dados do usuario mais a data para criacao de token de acesso

		if(!usuarioDto.getAtivo()) {
			throw new UsuarioException("Este usuário não está ativo no sistema");
		}
		
		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorUsuario(usuario);
		
		AcessoUsuarioDTO acessoUsuarioDto = dozerBeanMapper.map(acessoUsuario, AcessoUsuarioDTO.class);
		
		Calendar dataAtual = Calendar.getInstance();
		String tokenDadosUsuario = usuarioDto.getId() + acessoUsuarioDto.getEmail() + acessoUsuarioDto.getSenha() + dataAtual.getTimeInMillis();
		
		Senha token = new Senha();
		String retornoToken = token.criptografar(tokenDadosUsuario);
		
		TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDto = new TokenRecuperacaoSenhaDTO();
		
		tokenRecuperacaoDeSenhaDto.setUsuario(usuarioDto);
		tokenRecuperacaoDeSenhaDto.setToken(retornoToken);
		tokenRecuperacaoDeSenhaDto.setAtivo(Boolean.TRUE);
		tokenRecuperacaoDeSenhaDto.setDataRecuperacaoSenha(dataAtual);
		
		TokenRecuperacaoSenha tokenRecuperacaoDeSenha = dozerBeanMapper.map(tokenRecuperacaoDeSenhaDto, TokenRecuperacaoSenha.class);
		tokenRecuperacaoSenhaDao.save(tokenRecuperacaoDeSenha);
		
		return dozerBeanMapper.map(tokenRecuperacaoDeSenha, TokenRecuperacaoSenhaDTO.class);
	}

	@Transactional(readOnly = true)
	public TokenRecuperacaoSenhaDTO trazerDadosViaToken(String token) throws TokenException {
		TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDto = new TokenRecuperacaoSenhaDTO();
		
		tokenRecuperacaoDeSenhaDto.setToken(token);
		
		Mapper mapper = new DozerBeanMapper();
		TokenRecuperacaoSenha tokenRecuperacaoDeSenha = mapper.map(tokenRecuperacaoDeSenhaDto, TokenRecuperacaoSenha.class);
		
		tokenRecuperacaoDeSenha = tokenRecuperacaoSenhaDao.trazerDadosViaToken(tokenRecuperacaoDeSenha);
		
		if(tokenRecuperacaoDeSenha == null) {
			
			throw new TokenException("Token não existente. Por favor, refaça o processo de recuperação de senha.");
			
		} else if(tokenRecuperacaoDeSenha.getAtivo() == false) {
			
			throw new TokenException("Token inativo. Por favor, refaça o processo de recuperação de senha.");
		
		}
			
		return dozerBeanMapper.map(tokenRecuperacaoDeSenha, TokenRecuperacaoSenhaDTO.class);
	}
	
	
	@Transactional(propagation = Propagation.NESTED)
	public void cancelarToken(TokenRecuperacaoSenhaDTO tokenRecuperacaoDeSenhaDto) {
		
		tokenRecuperacaoDeSenhaDto.setAtivo(false);
		TokenRecuperacaoSenha tokenRecuperacaoSenha = dozerBeanMapper.map(tokenRecuperacaoDeSenhaDto, TokenRecuperacaoSenha.class);
		
		
		tokenRecuperacaoSenhaDao.update(tokenRecuperacaoSenha);
		tokenRecuperacaoSenhaDao.flush();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void inativarTokensPorHora() {
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, -1);
		
		TokenRecuperacaoSenha tokenRecuperacaoSenha = new TokenRecuperacaoSenha();
		tokenRecuperacaoSenha.setDataRecuperacaoSenha(calendar);
		
		List<TokenRecuperacaoSenha> arrayTokens = tokenRecuperacaoSenhaDao.obterTokensGeradosAposUmaHora(tokenRecuperacaoSenha);
		
		for(TokenRecuperacaoSenha token : arrayTokens) {
			
			token.setAtivo(false);
			tokenRecuperacaoSenhaDao.update(token);
		}
	}	
}
