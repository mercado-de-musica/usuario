package br.com.mercadodemusica.serviceImpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.entities.AcessoUsuario;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private AcessoUsuarioDao acessoUsuarioDao;
	
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorEmail(username);
        if (acessoUsuario == null){
            throw new UsernameNotFoundException("Usuário não encontrado no banco de dados");	
        }
        
        User springUser = null;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(acessoUsuario.getUsuario().getTipoUsuario().name()));
        
         
        springUser = new User(acessoUsuario.getEmail(), acessoUsuario.getSenha(), acessoUsuario.getUsuario().getAtivo(),
                accountNonExpired, credentialsNonExpired, accountNonLocked,
                authorities);

        return springUser;
        
        
	}

}
