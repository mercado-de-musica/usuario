package br.com.mercadodemusica.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.DocumentException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.WKTReader;

import br.com.mercadodemusica.autenticacoes.GoogleAPI;
import br.com.mercadodemusica.dao.AcessoUsuarioDao;
import br.com.mercadodemusica.dao.EnderecoDao;
import br.com.mercadodemusica.dao.ReputacaoVendedorDao;
import br.com.mercadodemusica.dao.TelefoneDao;
import br.com.mercadodemusica.dao.UsuarioCrawlerDao;
import br.com.mercadodemusica.dao.UsuarioDao;
import br.com.mercadodemusica.dao.UsuarioPessoaFisicaDao;
import br.com.mercadodemusica.dao.UsuarioPessoaJuridicaDao;
import br.com.mercadodemusica.dto.AcessoUsuarioDTO;
import br.com.mercadodemusica.dto.CidadeDTO;
import br.com.mercadodemusica.dto.EnderecoDTO;
import br.com.mercadodemusica.dto.EstadoDTO;
import br.com.mercadodemusica.dto.PaisDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.dto.ReputacaoVendedorDTO;
import br.com.mercadodemusica.dto.TelefoneDTO;
import br.com.mercadodemusica.dto.UsuarioCrawlerDTO;
import br.com.mercadodemusica.dto.UsuarioDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaFisicaDTO;
import br.com.mercadodemusica.dto.UsuarioPessoaJuridicaDTO;
import br.com.mercadodemusica.entities.AcessoUsuario;
import br.com.mercadodemusica.entities.Cidade;
import br.com.mercadodemusica.entities.Endereco;
import br.com.mercadodemusica.entities.Estado;
import br.com.mercadodemusica.entities.Pais;
import br.com.mercadodemusica.entities.ReputacaoVendedor;
import br.com.mercadodemusica.entities.Telefone;
import br.com.mercadodemusica.entities.Usuario;
import br.com.mercadodemusica.entities.UsuarioCrawler;
import br.com.mercadodemusica.entities.UsuarioPessoaFisica;
import br.com.mercadodemusica.entities.UsuarioPessoaJuridica;
import br.com.mercadodemusica.enums.PaisesEnum;
import br.com.mercadodemusica.enums.TipoDeTelefoneEnum;
import br.com.mercadodemusica.enums.TipoUsuarioEnum;
import br.com.mercadodemusica.exceptions.DocumentosException;
import br.com.mercadodemusica.exceptions.EmailException;
import br.com.mercadodemusica.exceptions.EnderecoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.exceptions.ReputacaoVendedorException;
import br.com.mercadodemusica.exceptions.TelefoneException;
import br.com.mercadodemusica.exceptions.TokenException;
import br.com.mercadodemusica.exceptions.UsuarioException;
import br.com.mercadodemusica.service.AcessoUsuarioService;
import br.com.mercadodemusica.service.UsuarioService;
import br.com.mercadodemusica.transformacaoDeDados.DatasTransformacao;
import br.com.mercadodemusica.transformacaoDeDados.Email;
import br.com.mercadodemusica.transformacaoDeDados.Paginacao;
import br.com.mercadodemusica.transformacaoDeDados.PointEndereco;
import br.com.mercadodemusica.transformacaoDeDados.Senha;
import br.com.mercadodemusica.transformacaoDeDados.TracosPontosBarras;
import br.com.mercadodemusica.validacoes.Cnpj;
import br.com.mercadodemusica.validacoes.Cpf;
import br.com.mercadodemusica.validacoes.StringNumero;

@Service
public class UsuarioServiceImpl implements UsuarioService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private UsuarioPessoaFisicaDao usuarioPessoaFisicaDao;
	
	@Autowired
	private UsuarioPessoaJuridicaDao usuarioPessoaJuridicaDao;
	
	@Autowired
	private UsuarioCrawlerDao usuarioCrawlerDao;
	
	@Autowired
	private TelefoneDao telefoneDao;
	
	@Autowired
	private EnderecoDao enderecoDao;
	
	@Autowired
	private ReputacaoVendedorDao reputacaoVendedorDao;
	
	@Autowired
	private AcessoUsuarioDao acessoUsuarioDao;
	
	@Autowired
	private AcessoUsuarioService acessoUsuarioService;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
//	private static final Integer QUANTIDADE_REGISTROS = 20;
	
	private final Logger logger = LogManager.getLogger(UsuarioServiceImpl.class);
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void cadastrarUsuarioPessoaFisicaOuJuridica(UsuarioDTO usuarioDto) throws Exception {
		
		if(usuarioDto instanceof UsuarioPessoaJuridicaDTO) {
			UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto = (UsuarioPessoaJuridicaDTO) usuarioDto;
			this.cadastrarUsuarioPessoaJuridica(usuarioPessoaJuridicaDto);
		} else if(usuarioDto instanceof UsuarioCrawlerDTO) {
			UsuarioCrawlerDTO usuarioCrawlerDto = (UsuarioCrawlerDTO) usuarioDto;
			this.cadastrarUsuarioCrawler(usuarioCrawlerDto);
		} else {
			UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto = (UsuarioPessoaFisicaDTO) usuarioDto;
			this.cadastrarUsuarioPessoaFisica(usuarioPessoaFisicaDto);
		}
		
		
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED)
	private void cadastrarUsuarioCrawler(UsuarioCrawlerDTO usuarioDto) throws EnderecoException, TelefoneException {
		
		
		
//		this.validacaoUsuario(usuarioCrawlerDto);
		
		usuarioDto.setTipoUsuario(TipoUsuarioEnum.ROLE_CRAWLER);
		
		UsuarioCrawler usuarioCrawler = dozerBeanMapper.map(usuarioDto, UsuarioCrawler.class);
		
		
//		neste ponto invertemos a ordem de inserts entre enderecos e usuario, evitando existir entidades transient 
		UsuarioCrawler usuarioCrawlerComId = null;
		if(usuarioCrawler.getId() == null) {
			
			usuarioCrawler.setDataDeCadastramento(Calendar.getInstance());
			usuarioCrawler.setDataDeUpdate(Calendar.getInstance());
			
			
			usuarioCrawlerComId = usuarioCrawlerDao.save(usuarioCrawler);	
			
//			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioCrawlerComId);
//			
//			this.atualizarEnderecos(listaDeEnderecos);
//			this.atualizarTelefones(listaDeTelefones);
		
		} else {
			
//			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioCrawler);
//			
//			this.atualizarEnderecos(listaDeEnderecos);
//			this.atualizarTelefones(listaDeTelefones);
			
			Usuario usuarioRetornadoBancoDeDados = usuarioDao.selectOne(usuarioCrawler.getId());
			
			usuarioCrawler.setDataDeUpdate(Calendar.getInstance());
			usuarioCrawler.setDataDeCadastramento(usuarioRetornadoBancoDeDados.getDataDeCadastramento());
			
			usuarioCrawlerDao.update(usuarioCrawler);
		}
		
	}




	@Transactional(readOnly = true)
	private void inserirUsuarioEmEnderecoTelefone(List<EnderecoDTO> listaDeEnderecosDto, List<TelefoneDTO> listaDeTelefonesDto, Usuario usuario) {
		UsuarioDTO usuarioDto = dozerBeanMapper.map(usuario, UsuarioDTO.class);
		
		for(EnderecoDTO enderecoDto : listaDeEnderecosDto) {
			enderecoDto.setUsuario(usuarioDto);
		}
		
		for(TelefoneDTO telefonesDto : listaDeTelefonesDto) {
			telefonesDto.setUsuario(usuarioDto);
		}
	}



	@Transactional(propagation = Propagation.REQUIRED)
	public void cadastrarUsuarioPessoaFisica(UsuarioPessoaFisicaDTO usuarioDto) throws IOException, InterruptedException, ParseException, RegraDeNegocioException {
			
		/*
		 * validacao CPF
		 */
		
		usuarioDto.setCpf(new TracosPontosBarras(usuarioDto.getCpf()).getString());
		if( usuarioDto.getCpf().length() != 11 ) {
			throw new DocumentosException("O CPF não contém 11 caracteres");
		} else if(! this.verificarCpfNaoExistente(usuarioDto.getCpf())) {
			throw new DocumentosException("Este CPF já está cadastrado no banco de dados ou é inválido");
		}
		
		
		/*
		 * validacao de RG
		 * 
		 */
		if(usuarioDto.getRg() == null || usuarioDto.getRg().isEmpty()) {
			throw new DocumentosException("O RG não pode estar vazio");
		}
				
		/*
		 * validacao Data de nascimento
		 */
//		usuarioDto.setDataDeNascimento(new DatasTransformacao().stringToCalendar(usuarioDto.getDataDeNascimentoString()));
//		if(usuarioDto.getDataDeNascimento() == null) {
//			throw new DocumentosException("Data de nascimento não está correta");
//		} else if(!this.verificarDataDeNascimento(usuarioDto.getDataDeNascimentoString())) {
//			throw new DocumentosException("É necessário ser maior de 18 anos para concluir o cadastro");
//		}
		
		
		
		/*
		 * mudancas de listas para HashSet
		 */
		
		
		this.validarEndereco(usuarioDto.getEnderecosList());
		
		this.validarTelefones(usuarioDto.getTelefonesList());
		
		AcessoUsuarioDTO acessoUsuarioDto = this.obterAcessoUsuario(usuarioDto);
		
		List<EnderecoDTO> listaDeEnderecos = usuarioDto.getEnderecosList();
		List<TelefoneDTO> listaDeTelefones = usuarioDto.getTelefonesList();
		
		UsuarioPessoaFisica usuarioPessoaFisica = dozerBeanMapper.map(usuarioDto, UsuarioPessoaFisica.class);
		
		
//		neste ponto invertemos a ordem de inserts entre enderecos e usuario, evitando existir entidades transient 
		UsuarioPessoaFisica usuarioPessoaFisicaComId = null;
		UsuarioPessoaFisicaDTO usuarioPessoaFisicaDto = null;
		if(usuarioPessoaFisica.getId() == null) {
			
			usuarioPessoaFisica.setDataDeCadastramento(Calendar.getInstance());
			usuarioPessoaFisica.setDataDeUpdate(Calendar.getInstance());
			
			
			usuarioPessoaFisicaComId = usuarioPessoaFisicaDao.save(usuarioPessoaFisica);	
			
			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioPessoaFisicaComId);
			
			this.atualizarEnderecos(listaDeEnderecos);
			this.atualizarTelefones(listaDeTelefones);
			
			usuarioPessoaFisicaDto = dozerBeanMapper.map(usuarioPessoaFisicaComId, UsuarioPessoaFisicaDTO.class);
		
		} else {
			
			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioPessoaFisica);
			
			this.atualizarEnderecos(listaDeEnderecos);
			this.atualizarTelefones(listaDeTelefones);
			
			Usuario usuarioRetornadoBancoDeDados = usuarioDao.selectOne(usuarioPessoaFisica.getId());
			
			usuarioPessoaFisica.setDataDeUpdate(Calendar.getInstance());
			usuarioPessoaFisica.setDataDeCadastramento(usuarioRetornadoBancoDeDados.getDataDeCadastramento());
			
			usuarioPessoaFisicaDao.update(usuarioPessoaFisica);
			
			usuarioPessoaFisicaDto = dozerBeanMapper.map(usuarioPessoaFisica, UsuarioPessoaFisicaDTO.class);
		}	
		
		acessoUsuarioDto.setUsuario(usuarioPessoaFisicaDto);
		acessoUsuarioService.inserirAcessoUsuario(acessoUsuarioDto);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void cadastrarUsuarioPessoaJuridica(UsuarioPessoaJuridicaDTO usuarioDto) throws IOException, InterruptedException, ParseException, RegraDeNegocioException {

		/*
		 * validacao Nome Fantasia
		 */
		if(usuarioDto.getNomeFantasia() == null || usuarioDto.getNomeFantasia().isEmpty()) {
			throw new DocumentosException("O nome fantasia não pode estar vazio");
		} else if(usuarioDto.getNomeFantasia().length() > 50) {
			throw new DocumentosException("O nome fantasia não pode ser maior que 50 caracteres");
		}
		
		/*
		 * validacao razao social
		 */
		
		if(usuarioDto.getRazaoSocial() == null || usuarioDto.getRazaoSocial().isEmpty()) {
			throw new DocumentosException("A razão sociial não pode estar vazia");
		} else if(usuarioDto.getRazaoSocial().length() > 100) {
			throw new DocumentosException("O nome fantasia não pode ser maior que 100 caracteres");
		}
		
		/*
		 * validacao CNPJ
		 */
		usuarioDto.setCnpj(new TracosPontosBarras(usuarioDto.getCnpj()).getString());
		if( usuarioDto.getCnpj().length() != 14 ) {
			throw new DocumentosException("O CNPJ não contém 14 caracteres");
		} else if(! this.verificarCnpjNaoExistente(usuarioDto.getCnpj())) {
			throw new DocumentosException("Este CNPJ já está cadastrado no banco de dados ou é inválido");
		}
		 
		
		/*
		 * validacao CPF titular
		 */
		
		usuarioDto.setCpfTitular(new TracosPontosBarras(usuarioDto.getCpfTitular()).getString());
		if( usuarioDto.getCpfTitular().length() != 11 ) {
			throw new DocumentosException("O CPF não contém 11 caracteres");
		} else if(! this.verificarCpfPessoaJuridicaNaoExistente(usuarioDto.getCpfTitular())) {
			throw new DocumentosException("Este CPF já está cadastrado no banco de dados ou é inválido");
		}
		
		
		/*
		 * validacao de RG
		 * 
		 */
		if(usuarioDto.getRgTitular() == null || usuarioDto.getRgTitular().isEmpty()) {
			throw new DocumentosException("O RG não pode estar vazio");
		}
		
		
		/*
		 * validacao Data de nascimento
		 */
//		if(usuarioDto != null && usuarioDto.getId() != null && usuarioDto.getDataDeNascimentoTitularString() == null) {
//			usuarioDto.setDataDeNascimentoTitularString(new DatasTransformacao().calendarToString(usuarioDto.getDataDeNascimentoTitular()));
//		}
//		
//		usuarioDto.setDataDeNascimentoTitular(new DatasTransformacao().stringToCalendar(usuarioDto.getDataDeNascimentoTitularString()));
//		if(usuarioDto.getDataDeNascimentoTitular() == null) {
//			throw new DocumentosException("Data de nascimento não está correta");
//		} else if (!this.verificarDataDeNascimento(usuarioDto.getDataDeNascimentoTitularString())) {
//			throw new DocumentosException("É necessário ser maior de 18 anos para concluir o cadastro");
//		}
		

		/*
		 * mudancas de listas para HashSet
		 */
		
		
		this.validarEndereco(usuarioDto.getEnderecosList());
		
		this.validarTelefones(usuarioDto.getTelefonesList());
		
		AcessoUsuarioDTO acessoUsuarioDto = this.obterAcessoUsuario(usuarioDto);
		
		List<EnderecoDTO> listaDeEnderecos = usuarioDto.getEnderecosList();
		List<TelefoneDTO> listaDeTelefones = usuarioDto.getTelefonesList();
		
		
		UsuarioPessoaJuridica usuarioPessoaJuridica = dozerBeanMapper.map(usuarioDto, UsuarioPessoaJuridica.class);
		
		
//		neste ponto invertemos a ordem de inserts entre enderecos e usuario, evitando existir entidades transient 
		UsuarioPessoaJuridica usuarioPessoaJuridicaComId = null;
		UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDto = null;
		if(usuarioPessoaJuridica.getId() == null) {
			
			usuarioPessoaJuridica.setDataDeCadastramento(Calendar.getInstance());
			usuarioPessoaJuridica.setDataDeUpdate(Calendar.getInstance());
			
			usuarioPessoaJuridicaComId = usuarioPessoaJuridicaDao.save(usuarioPessoaJuridica);
			
			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioPessoaJuridicaComId);
			
			this.atualizarEnderecos(listaDeEnderecos);
			this.atualizarTelefones(listaDeTelefones);
			
			usuarioPessoaJuridicaDto = dozerBeanMapper.map(usuarioPessoaJuridicaComId, UsuarioPessoaJuridicaDTO.class);
		
		} else {
			
			this.inserirUsuarioEmEnderecoTelefone(listaDeEnderecos, listaDeTelefones, usuarioPessoaJuridica);
			
			this.atualizarEnderecos(listaDeEnderecos);
			this.atualizarTelefones(listaDeTelefones);
			
			Usuario usuarioRetornadoBancoDeDados = usuarioDao.selectOne(usuarioPessoaJuridica.getId());
			
			usuarioPessoaJuridica.setDataDeUpdate(Calendar.getInstance());
			usuarioPessoaJuridica.setDataDeCadastramento(usuarioRetornadoBancoDeDados.getDataDeCadastramento());
			
			
			usuarioPessoaJuridicaDao.update(usuarioPessoaJuridica);
			
			usuarioPessoaJuridicaDto = dozerBeanMapper.map(usuarioPessoaJuridica, UsuarioPessoaJuridicaDTO.class);
			
		}
		
		
		acessoUsuarioDto.setUsuario(usuarioPessoaJuridicaDto);
		acessoUsuarioService.inserirAcessoUsuario(acessoUsuarioDto);		
	}

	@Transactional(readOnly = true)
	public Boolean verificarCpfNaoExistente(String cpf) throws RegraDeNegocioException {
			
		cpf = StringUtils.replace(cpf, "-", "");
		cpf = StringUtils.replace(cpf, ".", "");
		
		/*
		 * 
		 * Validacao aqui vale-se de novos cadastros e alteracoes. Verifica-se o id para saber se eh cadastro novo ou alteracao
		 * 
		 */
		
//		usuarioDao.obter
		cpf = new TracosPontosBarras(cpf).getString();
		
		if(cpf != null && !cpf.equals("")) {
			
			if(!Cpf.validaCpf(cpf)) {
				return false;
			}
			
			UsuarioPessoaFisica usuario = usuarioPessoaFisicaDao.selectPorCpf(cpf);
			if(usuario != null) {
				return false;
			}
		}
		
		return true;
	}
	
	@Transactional(readOnly = true)
	public Boolean verificarCnpjNaoExistente(String cnpj) throws RegraDeNegocioException {
		
		cnpj = StringUtils.replace(cnpj, "%2F", "/");
		cnpj = StringUtils.replace(cnpj, "-", "");
		cnpj = StringUtils.replace(cnpj, ".", "");
		cnpj = StringUtils.replace(cnpj, "/", "");
		
		/*
		 * 
		 * Validacao aqui vale-se de novos cadastros e alteracoes. Verifica-se o id para saber se eh cadastro novo ou alteracao
		 * 
		 */
		
		if(cnpj != null && !cnpj.equals("")) {
			
			Cnpj validacaoCnpj = new Cnpj();
			if(!validacaoCnpj.validaCnpj(cnpj)) {
				return false;
			}
			
			UsuarioPessoaJuridica usuario = usuarioPessoaJuridicaDao.selectPorCnpj(cnpj);
			if(usuario != null) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Boolean verificarCpfPessoaJuridicaNaoExistente(String cpfTitular) throws RegraDeNegocioException {
		
		cpfTitular = StringUtils.replace(cpfTitular, "-", "");
		cpfTitular = StringUtils.replace(cpfTitular, ".", "");
		
		/*
		 * 
		 * Validacao aqui vale-se de novos cadastros e alteracoes. Verifica-se o id para saber se eh cadastro novo ou alteracao
		 * 
		 */
		
		
		if(cpfTitular != null && !cpfTitular.equals("")) {
			
			if(!Cpf.validaCpf(cpfTitular)) {
				return false;
			}
			
			UsuarioPessoaJuridica usuario = usuarioPessoaJuridicaDao.selectPorCpfTitular(cpfTitular);
			if(usuario != null) {
				return false;
			}
		}
		
		return true;
		
		
	}
	
	@Transactional(readOnly = true)
	public UsuarioDTO obterInfosUsuario(String login) throws UsuarioException {
		
		if(login == null) {
			throw new UsuarioException("offline");
		}
		
		return this.informacoesDeUsuarioLogin(login);
	}
	

//	public UsuarioDTO setsToList(UsuarioDTO usuarioDTO) {
//		ArrayList<EnderecoDTO> enderecoListDto = new ArrayList<EnderecoDTO>();
//		Iterator<EnderecoDTO> iteracaoEndereco =  usuarioDTO.getEnderecos().iterator();
//		while(iteracaoEndereco.hasNext()) {
//			EnderecoDTO endereco = iteracaoEndereco.next();
//			enderecoListDto.add(endereco);
//		}
//		enderecoListDto.trimToSize();
//		
//		
//		ArrayList<TelefoneDTO> telefonesListDto = new ArrayList<TelefoneDTO>();
//		Iterator<TelefoneDTO> iteracaoTelefones = usuarioDTO.getTelefones().iterator();
//		while(iteracaoTelefones.hasNext()) {
//			TelefoneDTO telefone = iteracaoTelefones.next();
//			telefonesListDto.add(telefone);
//		}
//		telefonesListDto.trimToSize();
//		usuarioDTO.setTelefonesList(telefonesListDto);
//		
//		if(usuarioDTO instanceof UsuarioPessoaFisicaDTO) {
//			Calendar dataDeNascimento = ((UsuarioPessoaFisicaDTO) usuarioDTO).getDataDeNacimento();
//			SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
//			((UsuarioPessoaFisicaDTO) usuarioDTO).setDataDeNascimentoString(formatoData.format(dataDeNascimento.getTime())); 
//		}
//		
//		
//		usuarioDTO.setEnderecosList(enderecoListDto);
//		return usuarioDTO;
//	}
	
	
	@Transactional(readOnly = true)
	public UsuarioDTO obterInfosUsuarioPorCpfCnpj(String cpfCnpj) throws DocumentException, RegraDeNegocioException {
		
		
		UsuarioDTO usuarioDto = null;
		if( cpfCnpj!= null && !cpfCnpj.isEmpty() && cpfCnpj.length() == 11 ) {
			
			UsuarioPessoaFisicaDTO usuarioFisicoDto = new UsuarioPessoaFisicaDTO();
			usuarioFisicoDto.setCpf(cpfCnpj);
			Boolean retornoVerificacaoUsuario = this.verificarCpfNaoExistente(usuarioFisicoDto.getCpf());
			
			
			if(!retornoVerificacaoUsuario) {
				UsuarioPessoaFisica usuarioEntity = usuarioPessoaFisicaDao.selectPorCpf(usuarioFisicoDto.getCpf());
				
				Mapper mapper = new DozerBeanMapper();
				usuarioDto = mapper.map(usuarioEntity, UsuarioPessoaFisicaDTO.class);
				
				return usuarioDto;
				
			} else {
				throw new UsuarioException("Não foi encontrado usuário ligado a este CPF");
			}
			
			
		} else if( cpfCnpj!= null && !cpfCnpj.isEmpty() && cpfCnpj.length() == 14 ) {
			
			UsuarioPessoaJuridicaDTO usuarioJuridicoDto = new UsuarioPessoaJuridicaDTO();
			usuarioJuridicoDto.setCnpj(cpfCnpj);
			Boolean retornoVerificacaoUsuario = this.verificarCnpjNaoExistente(usuarioJuridicoDto.getCnpj());
			
			if(!retornoVerificacaoUsuario) {
				UsuarioPessoaJuridica usuarioEntity = usuarioPessoaJuridicaDao.selectPorCnpj(usuarioJuridicoDto.getCnpj());
				
				Mapper mapper = new DozerBeanMapper();
				usuarioDto = mapper.map(usuarioEntity, UsuarioPessoaJuridicaDTO.class);
				
				return usuarioDto;
				
			} else {
				throw new UsuarioException("Não foi encontrado usuário ligado a este CNPJ");
			}
			
		} else {
			throw new DocumentException("A numeração está errada. Por favor, tente de novo.");
		}
	}

	@Override
	@Transactional(readOnly = true)
	public UsuarioDTO obterUsuarioEnderecoPesquisaProdutos(JSONObject jsonObject) throws UsuarioException, JSONException, JsonParseException, JsonMappingException, IOException, EnderecoException, com.vividsolutions.jts.io.ParseException {
		
		
		/*
		 * verificacao se o usuario nao esta logado, Pegamos pelo IP a cidade para fazer a pesquisa.
		 * Se o Ip nao trazer nenhum dado, retornamos null e o modulo de produtos faz uma query generica.
		 * 
		 * Se caso o usuario estiver logado, será feito também uma query de acordo com o endereco dele
		 */
		JSONObject retornoJson = null;
		
		if(jsonObject.length() == 0 || (jsonObject.length() != 0 && jsonObject.isNull("remoteUser") && jsonObject.isNull("usuarioDto"))) {
			
			logger.info("usuario nulo, vai procurar o endereco do caboclo");
			
			String enderecoIp;
			try {
				
				enderecoIp = this.whatsMyIp();
				
				 //you get the IP as a String
				
				
				logger.info("ip do usuario:" + enderecoIp);
				
				return this.obterLatitudeLongitude(enderecoIp, retornoJson);
				
			} catch (Exception e) {
				
				logger.info("usuario retornou vazio");
				return null;
			
			}

		} else if(jsonObject.isNull("remoteUser") && !jsonObject.isNull("usuarioDto")) {
			
			ObjectMapper objectMapper = new ObjectMapper();
			UsuarioDTO usuarioDto = objectMapper.readValue(jsonObject.getString("usuarioDto"), UsuarioDTO.class);

		   return usuarioDto;
		
		} else {
			
			logger.info("usuario retornou com alguma coisa");
			return this.obterLatitudeLongitude(jsonObject.getString("remoteUser"), retornoJson);
			
//			return this.obterInfosUsuario(jsonObject.getString("remoteUser"));
		
		}
		
		
				
//		procurar cidade pelo ip
//		http://freegeoip.net/json/179.209.62.133
//		{"ip":"179.209.62.133","country_code":"BR","country_name":"Brasil","region_code":"SP","region_name":"SÃ£o Paulo","city":"SÃ£o Paulo","zip_code":"","time_zone":"America/Sao_Paulo","latitude":-23.548,"longitude":-46.637,"metro_code":0}	
		
	}

	@Override
	@Transactional(readOnly = true)
	public UsuarioDTO obterInfosUsuarioPorUsuario(UsuarioDTO usuarioDto) {
		
		Usuario usuarioEntidade = dozerBeanMapper.map(usuarioDto, Usuario.class);
		usuarioEntidade = usuarioDao.selectOne(usuarioEntidade.getId());
		
		return dozerBeanMapper.map(usuarioEntidade, UsuarioDTO.class);
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void cadastrarReputacaoDoVendedor(ReputacaoVendedorDTO reputacaoVendedorDto) throws ReputacaoVendedorException {
		ReputacaoVendedor reputacaoVendedor = dozerBeanMapper.map(reputacaoVendedorDto, ReputacaoVendedor.class);
		
		if(reputacaoVendedorDto.getNota() == null || reputacaoVendedorDto.getNota().equals("")) {
			throw new ReputacaoVendedorException("A nota não pode estar vazia");
		}
		
		if(reputacaoVendedorDto.getMensagem() != null && reputacaoVendedorDto.getMensagem().equals("")) {
			reputacaoVendedorDto.setMensagem(null);
		}
		
		reputacaoVendedorDao.update(reputacaoVendedor);
	}




	@Override
	@Transactional(readOnly = true)
	public String verificarReputacaoVendedor(ProdutoDTO infosGeraisProdutoUsuarioDto) {
		
		Usuario usuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto.getUsuario(), Usuario.class);
		
		List<ReputacaoVendedor> listaDeReputacao = reputacaoVendedorDao.obterReputacaoPorUsuario(usuario);
		
		Integer contagem = Integer.valueOf(0);
		Integer nota = Integer.valueOf(0);
		for(ReputacaoVendedor reputacaoVendedor : listaDeReputacao) {
			if(reputacaoVendedor != null) {
				contagem++;
				nota = nota.intValue() + reputacaoVendedor.getNota().intValue();
			}
		}
		
		if(contagem.equals(Integer.valueOf(0))) {
			
			return "100";
		
		} else {
			
			double notaDividida = nota.doubleValue() / contagem.doubleValue();
			Double notaFinal = notaDividida * 10;
			
			return String.valueOf(notaFinal.intValue());
		}
		
	}
	
	@Override
	public Boolean verificarDataDeNascimento(String dataDeNascimentoString) {
		dataDeNascimentoString = StringUtils.replace(dataDeNascimentoString, "%2F", "/");
		Calendar dataDeNascimento = new DatasTransformacao().stringToCalendar(dataDeNascimentoString);
		
		if(dataDeNascimento == null) {
			return false;
		}
		
		Calendar dezoitoAnosAtras = Calendar.getInstance();	
		dezoitoAnosAtras.add(Calendar.YEAR, -18);		
		
		if(dataDeNascimento.after(dezoitoAnosAtras)) {
			return false;
		} else {
			return true;
		}		
	}
	
	
	@Override
	public void obterInfosUsuarioVerificacaoOnLine(HttpServletRequest request) throws UsuarioException {
		
		if(request.getRemoteUser() == null) {
			request.getSession().setAttribute("urlCompraDeProduto", request.getHeader("Referer").toString());
			throw new UsuarioException("offline");
		}
		
		this.informacoesDeUsuarioLogin(request.getRemoteUser());
	}
	
	@Override
	public void verificacaoCpfCnpj(String cpfCnpj) throws DocumentosException {
		
		
		if(cpfCnpj.length() == 14) {
			Cnpj cnpj = new Cnpj();
			Boolean retornoCnpj = cnpj.validaCnpj(cpfCnpj);
			if(!retornoCnpj) {
				throw new DocumentosException("CNPJ está escrito de forma errada");
			}
		} else if(cpfCnpj.length() == 11){	
			Boolean retornoCpf = Cpf.validaCpf(cpfCnpj);
			if(!retornoCpf) {
				throw new DocumentosException("CPF está escrito de forma errada");
			}
		} else {
			throw new DocumentosException("O número do seu documento está errado");
		}
	}


	@Override
	public void transformacaoDeData(UsuarioDTO usuarioDto) {
		
		if(usuarioDto instanceof UsuarioPessoaJuridicaDTO) {
			
			UsuarioPessoaJuridicaDTO pessoaJuridicaDto = (UsuarioPessoaJuridicaDTO) usuarioDto;	
			pessoaJuridicaDto.setDataDeNascimentoTitularString(new DatasTransformacao().calendarToString(pessoaJuridicaDto.getDataDeNascimentoTitular()));
		
		} else {

			UsuarioPessoaFisicaDTO pessoaFisicaDto = (UsuarioPessoaFisicaDTO) usuarioDto;
			pessoaFisicaDto.setDataDeNascimentoString(new DatasTransformacao().calendarToString(pessoaFisicaDto.getDataDeNascimento()));
		
		}
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void liberarUsuario(UsuarioDTO usuarioDto) throws TokenException {
		
		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
		
		usuario.setAtivo(Boolean.TRUE);
		if(!usuarioDao.contains(usuario)) {
			usuarioDao.merge(usuario);
		} else {
			usuarioDao.save(usuario);
		}
	}




//	@Override
//	@Transactional(readOnly = true)
//	public List<ReputacaoVendedorDTO> obterReputacaoVendedorProduto(List<CompraDTO> listaDeComprasFinalizadas) {
//		List<ReputacaoVendedorDTO> listaReputacaoVendedorDto = new ArrayList<ReputacaoVendedorDTO>();
//		for(CompraDTO compraFinalizadaDto : listaDeComprasFinalizadas) {
//			
//			Compra compra = dozerBeanMapper.map(compraFinalizadaDto, Compra.class);
//			
//			
//			
//			for(ProdutoDTO produtoCompradoDto : compraFinalizadaDto.getProdutosComprados()) {
//					
//				Produto infosGeraisProdutoUsuario = 
//				
//				Usuario usuarioComprador = dozerBeanMapper.map(compraFinalizadaDto.getComprador(), Usuario.class);
//				ReputacaoVendedor reputacaoVendedor = reputacaoVendedorDao.obterReputacaoPorProdutoCompradoCompradorVendedor(infosGeraisProdutoUsuario, usuarioComprador);
//				
//				if(reputacaoVendedor != null) {	
//					ReputacaoVendedorDTO reputacaoVendedorDto = dozerBeanMapper.map(reputacaoVendedor, ReputacaoVendedorDTO.class);
//					
//					listaReputacaoVendedorDto.add(reputacaoVendedorDto);
////					produtoCompradoDto.getUsuario().setReputacaoVendedor(listaReputacaoVendedorDto);
//				}				
//			}
//		}
//		
//		return listaReputacaoVendedorDto;
//	}
	
	@Override
	@Transactional(readOnly = true)
	public UsuarioDTO obterInfosUsuarioPorId(BigInteger idUsuario) {
		
		Usuario usuarioEntidade = usuarioDao.selectOne(idUsuario);
		
		if(usuarioEntidade != null) {
			return dozerBeanMapper.map(usuarioEntidade, UsuarioDTO.class);
		}
		
		return null;
	
	}




//	@Override
//	public String obterNomeDaLojaOnline(UsuarioDTO usuarioDonoDaLojaDto, List<InstrumentoAcessorioLojaOnlineDTO> listaInstrumentoAcessorioLojaOnlineDto) {
//		
//		String nomeDaLoja = "";
//		if(!NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDonoDaLojaDto).equals("") && listaInstrumentoAcessorioLojaOnlineDto != null && !listaInstrumentoAcessorioLojaOnlineDto.isEmpty()) {
//			nomeDaLoja = "Loja de " + NomePessoaJuridicaOuFisica.obterNomeDto(usuarioDonoDaLojaDto);
//			
//			Iterator<EnderecoDTO> enderecoIterator = usuarioDonoDaLojaDto.getEnderecos().iterator();
//			while(enderecoIterator.hasNext()) {
//				EnderecoDTO endereco = enderecoIterator.next();
//				
//				if(endereco.getEnderecoDefault()) {
//					nomeDaLoja = nomeDaLoja + " - " + endereco.getCidade().getNome() + " / " + endereco.getCidade().getEstado().getNome();
//				}
//			}
//		}
//	
//		return nomeDaLoja;
//	}

//	@Override
//	@Transactional(readOnly = true)
//	public List<LojaOnlineDTO> obterLojasOnline(UsuarioDTO usuarioDto) throws EnderecoException {
//		
//		List<LojaOnlineDTO> listaLojaOnlineDto = new ArrayList<LojaOnlineDTO>();
//		
//		List<UsuarioDTO> listaUsuarioDto = this.obterUsuariosPorProximidadeComProdutos(usuarioDto, QUANTIDADE_REGISTROS);
//		
//		for(UsuarioDTO usuarioBancoDeDadosDto : listaUsuarioDto) {
//			LojaOnlineDTO lojaOnlineDto = new LojaOnlineDTO();
//			
//			lojaOnlineDto.setNomeDaLoja(NomePessoaJuridicaOuFisica.obterNomeDto(usuarioBancoDeDadosDto));
//			lojaOnlineDto.setIdLoja(usuarioBancoDeDadosDto.getId());
//			listaLojaOnlineDto.add(lojaOnlineDto);
//		}
//		
//		return listaLojaOnlineDto;
//	}


	@Override
	@Transactional(readOnly = true)
	public List<UsuarioDTO> obterUsuariosPorProximidadeComProdutos(UsuarioDTO usuarioDto, Integer quantidadeRegistros) throws RegraDeNegocioException {
		List<UsuarioDTO> listaDeUsuariosRetornoDto = new ArrayList<UsuarioDTO>();
		
		if(usuarioDto != null && usuarioDto.getId() != null) {
//			usuario online
			
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
			
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(usuario, endereco, quantidadeRegistros, null, null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
		
		} else if(usuarioDto != null && usuarioDto.getId() == null) {
//			usuario latitude longitude
			
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(endereco, quantidadeRegistros, null, null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
		
		} else {
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosComProdutos(quantidadeRegistros, null, null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
		}
		return listaDeUsuariosRetornoDto;
		
		
	}
	

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioDTO> obterLojasOnlinePorNome(UsuarioDTO usuarioDto, String nome) throws RegraDeNegocioException {
		List<UsuarioDTO> listaDeUsuariosRetornoDto = new ArrayList<UsuarioDTO>();
		
//		usuario online
		if(usuarioDto != null && usuarioDto.getId() != null) {
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
			
			List<BigInteger> listaDeIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(usuario, endereco, null, null, nome);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaDeIds, listaDeUsuariosRetornoDto);
			
		} else if(usuarioDto != null && usuarioDto.getId() == null){
//			chamada de usuario com latitude e longitude
			
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			List<BigInteger> listaDeIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(endereco, null, null, nome);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaDeIds, listaDeUsuariosRetornoDto);
			
		} else {
//			quando nao existe nenhum usuario
			List<BigInteger> listaDeIds = usuarioDao.obterIdsUsuariosComProdutos(null, null, nome);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaDeIds, listaDeUsuariosRetornoDto);
		}
		
		
		return listaDeUsuariosRetornoDto;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> obterListaDeLojasOnLine(UsuarioDTO usuarioDto, BigInteger quantidadeRegistros, BigInteger pagina) throws RegraDeNegocioException {
		List<UsuarioDTO> listaDeUsuariosRetornoDto = new ArrayList<UsuarioDTO>();
		Map<String, Object> hashMap = new HashMap<String, Object>();
		
		
		BigInteger totalLojas = null;
		if(usuarioDto != null && usuarioDto.getId() != null) {
//			usuarioLogado
			
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
			
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(usuario, endereco, quantidadeRegistros.intValue(), pagina.intValue(), null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
			
			totalLojas = usuarioDao.obterContagemUsuariosPorProximidadeDeEnderecoComProdutosUsuarioOnline(usuario, endereco, null);
			
		} else if(usuarioDto != null && usuarioDto.getId() == null) {
//			usuario latitude longitude
			
			EnderecoDTO enderecoDto = this.obterEnderecoPorUsuarioDto(usuarioDto);
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosPorProximidadeDeEnderecoComProdutos(endereco, quantidadeRegistros.intValue(), pagina.intValue(), null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
			
			totalLojas = usuarioDao.obterContagemUsuariosPorProximidadeDeEnderecoComProdutos(endereco, null);
		} else {
//			usuario sem laTitude
			List<BigInteger> listaIds = usuarioDao.obterIdsUsuariosComProdutos(quantidadeRegistros.intValue(), pagina.intValue(), null);
			this.colocarUsuariosListaDeRetornoLojaOnLine(listaIds, listaDeUsuariosRetornoDto);
			
			totalLojas = usuarioDao.obterContagemUsuariosComProdutos(null);
		}
		
		hashMap.put("lista", listaDeUsuariosRetornoDto);
		hashMap.put("contagem", totalLojas);
		hashMap.put("paginas", Paginacao.paginas(totalLojas, quantidadeRegistros));
		hashMap.put("lojasPorPagina", quantidadeRegistros);
		hashMap.put("paginaAtiva", pagina);
		hashMap.put("produtosPorPagina", quantidadeRegistros);
		
		return hashMap;
	}
	
	
	@Transactional(readOnly = true)
	private void verificarCidadeEstadoPais(Endereco endereco) {
		
		// procura em cascata. Caso não exista a primeira procura, vai procurando nos pais
		
		Cidade cidade = enderecoDao.obterCidadePorNome(endereco.getCidade().getNome());
		if(cidade != null) {
			endereco.setCidade(cidade);
		} else {
			Estado estado = enderecoDao.obterEstadoPorNome(endereco.getCidade().getEstado().getNome());
			if(estado != null) {
				endereco.getCidade().setEstado(estado);
			} else {
				Pais pais = enderecoDao.obterPaisPorEnum(endereco.getCidade().getEstado().getPais().getPaisEnum());
				if(pais != null) {
					endereco.getCidade().getEstado().setPais(pais);
				}
			}
		}
	}
	
	public void validarEndereco(List<EnderecoDTO> enderecos) throws RegraDeNegocioException {
		for(EnderecoDTO enderecoDto : enderecos) {
			
			enderecoDto.setCep(new TracosPontosBarras(enderecoDto.getCep()).getString());
			
			if(enderecoDto.getCep() == null || enderecoDto.getCep().isEmpty()) {
				throw new EnderecoException("O CEP não pode ser vazio");
			}
			
			if(enderecoDto.getLogradouro() == null || enderecoDto.getLogradouro().isEmpty()) {
				throw new EnderecoException("O Logradouro não pode ser vazio");
			}
			
			if(enderecoDto.getNumero() == null || enderecoDto.getNumero().isEmpty() || ! new StringNumero().verificarStringSaoNumeros(enderecoDto.getNumero())) {
				throw new EnderecoException("A numeração não pode estar vazia ou ter letras");
			}
			
			if(enderecoDto.getCidade() == null || enderecoDto.getCidade().getNome().isEmpty()) {
				throw new EnderecoException("A cidade não pode estar vazia");
			}
			
			if(enderecoDto.getCidade().getEstado() == null || enderecoDto.getCidade().getEstado().getNome().isEmpty()) {
				throw new EnderecoException("A cidade não pode estar vazia");
			}
			
			PaisDTO paisDto = new PaisDTO();
			paisDto.setPaisEnum(PaisesEnum.BR);
			enderecoDto.getCidade().getEstado().setPais(paisDto);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	private AcessoUsuarioDTO obterAcessoUsuario(UsuarioDTO usuario) throws RegraDeNegocioException {
		
		/*
		 * validacao E-mail
		 */
		
		if(usuario.getEmail() == null || usuario.getEmail().isEmpty()) {
			throw new EmailException("O e-mail não pode ser vazio");
		}
		
		if((usuario.getSenha() == null || usuario.getSenha().isEmpty()) && usuario.getId() == null) {
			throw new UsuarioException("A senha não pode estar vazia");
		}
		
		Pattern pattern = Pattern.compile(Email.getPattern());  
		Matcher matcher = pattern.matcher(usuario.getEmail());  
		Boolean matchFound = matcher.matches();  
		if(!matchFound) {
			throw new EmailException("O e-mail está escrito de forma incorreta");
		}
		
		AcessoUsuarioDTO acessoUsuarioDto = this.obterAcessoUsuarioPorEmail(usuario);
		if(acessoUsuarioDto == null) {
			acessoUsuarioDto = new AcessoUsuarioDTO();
		}
				
		acessoUsuarioDto.setEmail(usuario.getEmail().toLowerCase());
		acessoUsuarioDto.setSenha(new Senha().criptografar(acessoUsuarioDto.getSenha() + acessoUsuarioDto.getEmail()));
		acessoUsuarioDto.setSenha(usuario.getSenha());
		
		
//		defaults para ativo e tipo de usuario
		if(usuario.getAtivo() == null) {
			usuario.setAtivo(false);
		}
		
		if(usuario.getTipoUsuario() == null) {
			usuario.setTipoUsuario(TipoUsuarioEnum.ROLE_USER);
		}
		
		
		return acessoUsuarioDto;
		
	}

	@Transactional(readOnly = true)
	private AcessoUsuarioDTO obterAcessoUsuarioPorEmail(UsuarioDTO usuarioDto) {
		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorEmail(usuarioDto.getEmail());
		
		if(acessoUsuario != null) {
			return dozerBeanMapper.map(acessoUsuario, AcessoUsuarioDTO.class);
		} else {
			return null;
		}
		
	}




	public void validarTelefones(List<TelefoneDTO> telefonesList) throws RegraDeNegocioException {
		
		/*
		 * validacao de telefones
		 * 
		 */
		
		for(TelefoneDTO telefone : telefonesList) {
						
			telefone.setDdd(new TracosPontosBarras(telefone.getDdd()).getString());
			if(telefone.getDdd() == null || telefone.getDdd().isEmpty()) {
				throw new TelefoneException("O ddd não pode ser vazio");
			}
			
			telefone.setTelefone(new TracosPontosBarras(telefone.getTelefone()).getString());
			if(telefone.getTelefone() == null || telefone.getTelefone().isEmpty()) {
				throw new TelefoneException("O ddd não pode ser vazio");
			}
			
			if(telefone.getTipoDeTelefone() == null) {
				throw new TelefoneException("O tipo de telefone não pode ser vazio");
			}
			
			
			String novoDdd = new TracosPontosBarras(telefone.getDdd()).getString();
			String novoTelefone = new TracosPontosBarras(telefone.getTelefone()).getString();
			
			if(novoDdd.length() != 2 || ! new StringNumero().verificarStringSaoNumeros(novoDdd)) {
				throw new TelefoneException("O DDD "+ novoDdd +" está escrito de forma incorreta. deve conter apenas 2 dígitos. Ex.: 21");
			}
			
			if((!(novoTelefone.length() >= 8 && novoTelefone.length() <= 9)) || ! new StringNumero().verificarStringSaoNumeros(novoTelefone)) {
				throw new TelefoneException("O telefone "+ novoTelefone +" está escrito de forma incorreta. deve conter entre 8 e 9 dígitos. Ex.: 4444-4444");
			}
			
			if( telefone.getTipoDeTelefone() != TipoDeTelefoneEnum.CELULAR && telefone.getTipoDeTelefone() != TipoDeTelefoneEnum.RESIDENCIA && 
					telefone.getTipoDeTelefone() != TipoDeTelefoneEnum.TRABALHO ) {
				throw new TelefoneException("O tipo de telefone está incorreto. Por favor, escolha um corretamente usando a lista diponível");
			}
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void atualizarTelefones(List<TelefoneDTO> listaDeTelefones) {

		Usuario usuario = dozerBeanMapper.map(listaDeTelefones.get(0).getUsuario(), Usuario.class);
		
		List<Telefone> telefonesExistentesDatabase = telefoneDao.obterTelefonesPorUsuario(usuario);
		
		ArrayList<BigInteger> telefonesExistentes = new ArrayList<BigInteger>();
		for(TelefoneDTO telefoneDto : listaDeTelefones) {
			Telefone telefone = dozerBeanMapper.map(telefoneDto, Telefone.class);
			
			if(telefone.getId() == null) {
				Telefone telefoneRetorno = telefoneDao.save(telefone);
				telefonesExistentes.add(telefoneRetorno.getId());
			} else {
				Telefone telefoneRetorno = telefoneDao.update(telefone);
				telefonesExistentes.add(telefoneRetorno.getId());
			}
		}
		
		
		for(Telefone telefonesDatabase : telefonesExistentesDatabase) {
			if(! telefonesExistentes.contains(telefonesDatabase.getId())) {
				telefoneDao.delete(telefonesDatabase);	
			}	
		}
		
		
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void atualizarEnderecos(List<EnderecoDTO> listaDeEnderecos) throws EnderecoException {
		Boolean primeiro = true;
		
		Usuario usuario = dozerBeanMapper.map(listaDeEnderecos.get(0).getUsuario(), Usuario.class);
		
		List<Endereco> enderecosExistentesDatabase = enderecoDao.obterEnderecosPorUsuario(usuario);
		
		ArrayList<BigInteger> enderecosExistentes = new ArrayList<BigInteger>();
		for(EnderecoDTO enderecoDto : listaDeEnderecos) {
//			Endereco endereco = iteratorEndereco.next();
			
			Endereco endereco = dozerBeanMapper.map(enderecoDto, Endereco.class);
			
//			mercadodemusica-1022
			GeoApiContext context = new GeoApiContext().setApiKey(GoogleAPI.API_KEY);
			
			GeocodingResult[] results;
			try {
				results = GeocodingApi.geocode(context, endereco.getNumero() + " " + endereco.getLogradouro() + ", " +endereco.getCidade().getNome() + " " +endereco.getCep()).await();
				
				if(results.length == 0) {
					throw new EnderecoException("Não foi possível buscar sua geolocalização. Por favor entre em contato conosco");
					
//					this.pegarGeolocalizacaoGeoLity(usuario);
				}
				
				BigDecimal latitudeBigDecimal = new BigDecimal(results[0].geometry.location.lat);
				BigDecimal longitudeBigDecimal = new BigDecimal(results[0].geometry.location.lng);
				
				WKTReader fromText = new WKTReader();
		        Geometry geom = fromText.read("POINT(" + longitudeBigDecimal + " " + latitudeBigDecimal + ")");
		        
		        if(!geom.getGeometryType().equals("Point")) {
		        	throw new EnderecoException("A geometria não é um ponto. Ela é um " + geom.getGeometryType() + "e não pode ser feito pesquisas sem a localização");
		        }
		        
		        endereco.setPosicao((Point) geom);
		        endereco.setLatitude(latitudeBigDecimal);
		        endereco.setLongitude(longitudeBigDecimal);

				
				if(endereco.getEnderecoDefault() == null && primeiro) {
					endereco.setEnderecoDefault(true);
					primeiro = false;
				}
					
				this.verificarCidadeEstadoPais(endereco);
			
				if(endereco.getId() == null) {
					Endereco enderecoRetorno = enderecoDao.save(endereco);
					enderecosExistentes.add(enderecoRetorno.getId());
					
				} else {
					Endereco enderecoRetorno = enderecoDao.update(endereco);
					enderecosExistentes.add(enderecoRetorno.getId());
				}
		        
			} catch (Exception e) {
				throw new EnderecoException(e);
			}
			
		}
		
		for(Endereco enderecoDatabase : enderecosExistentesDatabase) {
			if(! enderecosExistentes.contains(enderecoDatabase.getId())) {
				enderecoDao.delete(enderecoDatabase);	
			}	
		}
		
	}

	@Transactional(readOnly = true)
	private UsuarioDTO informacoesDeUsuarioLogin(String login) {

		AcessoUsuario acessoUsuario = acessoUsuarioDao.obterPorEmail(login);
		AcessoUsuarioDTO acessoUsuarioDto = dozerBeanMapper.map(acessoUsuario, AcessoUsuarioDTO.class);

		return acessoUsuarioDto.getUsuario();
		
	}	
	
	
	
	private String whatsMyIp() throws IOException {
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
		String enderecoIp = in.readLine();
		return enderecoIp;
	}
	
	
	
	@Transactional(readOnly = true)
	private void colocarUsuariosListaDeRetornoLojaOnLine(List<BigInteger> listaIds, List<UsuarioDTO> listaDeUsuariosRetornoDto) {
		for(BigInteger idUsuario : listaIds) {
			Usuario usuarioRetornoBancoDeDados = usuarioDao.selectOne(idUsuario);
			
			UsuarioDTO usuarioRetornoBancoDeDadosDto = dozerBeanMapper.map(usuarioRetornoBancoDeDados, UsuarioDTO.class);
			if(usuarioRetornoBancoDeDadosDto instanceof UsuarioPessoaJuridicaDTO) {
				UsuarioPessoaJuridicaDTO usuarioRetornoPessoaJuridicaBancoDeDadosDto = (UsuarioPessoaJuridicaDTO) usuarioRetornoBancoDeDadosDto;
				
				UsuarioPessoaJuridicaDTO usuarioPessoaJuridicaDTO = new UsuarioPessoaJuridicaDTO();
				usuarioPessoaJuridicaDTO.setRazaoSocial(usuarioRetornoPessoaJuridicaBancoDeDadosDto.getRazaoSocial());
				usuarioPessoaJuridicaDTO.setNomeFantasia(usuarioRetornoPessoaJuridicaBancoDeDadosDto.getNomeFantasia());
				usuarioPessoaJuridicaDTO.setId(usuarioRetornoPessoaJuridicaBancoDeDadosDto.getId());
				
				listaDeUsuariosRetornoDto.add(usuarioRetornoBancoDeDadosDto);
				
			} else {
				UsuarioPessoaFisicaDTO usuarioRetornoPessoaFisicaBancoDeDadosDto = (UsuarioPessoaFisicaDTO) usuarioRetornoBancoDeDadosDto;
				
				UsuarioPessoaFisicaDTO usuarioPessoaFisicaDTO = new UsuarioPessoaFisicaDTO();	
				usuarioPessoaFisicaDTO.setId(usuarioRetornoPessoaFisicaBancoDeDadosDto.getId());
				usuarioPessoaFisicaDTO.setNome(usuarioRetornoPessoaFisicaBancoDeDadosDto.getNome());
				usuarioPessoaFisicaDTO.setSobrenome(usuarioRetornoPessoaFisicaBancoDeDadosDto.getSobrenome());
				
				listaDeUsuariosRetornoDto.add(usuarioRetornoBancoDeDadosDto);
			}	
		}
	}


	@Transactional(readOnly = true)
	private EnderecoDTO obterEnderecoPorUsuarioDto(UsuarioDTO usuarioDto) throws EnderecoException {
		
		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
		
		List<Endereco> enderecos = enderecoDao.obterEnderecosPorUsuario(usuario);
		
		
		
		Endereco enderecoRetorno = null;
		for(Endereco endereco : enderecos) {
			if(endereco.getEnderecoDefault()) {
				EnderecoDTO enderecoDto = dozerBeanMapper.map(endereco, EnderecoDTO.class);
				return enderecoDto;
			} else {
				enderecoRetorno = endereco;
			}
		}
		
		if(enderecoRetorno == null) {
			throw new EnderecoException("ao foi possível encontrar o endereço");
		}
		
		return dozerBeanMapper.map(enderecoRetorno, EnderecoDTO.class);
	}
	
	
	
	private UsuarioDTO obterLatitudeLongitude(String enderecoIp, JSONObject retornoJson) throws IOException, JSONException, EnderecoException, com.vividsolutions.jts.io.ParseException {
		URL url = new URL("http://ip-api.com/json/" + enderecoIp);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();            
		connection.addRequestProperty("Request-Method","GET");      
		connection.setRequestProperty("accept", "application/json");
		  
		connection.setDoInput(true);    
		connection.setDoOutput(false);    
		connection.connect();    
		       
		if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {    
		    
			logger.info("encontrou endereco do usuario nulo");
			
			
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));    
		    String newData = "";    
		    String s = "";    
		    while (null != ((s = br.readLine()))) {    
		        newData += s;    
		    }    
		    
		    br.close();    
		    
		    retornoJson = new JSONObject(newData);
		}
		
		if ( ! retornoJson.get("region").equals("") && ! retornoJson.get("city").equals("") && ! retornoJson.get("countryCode").equals("") ){
		
			PaisDTO paisDto = new PaisDTO();
			paisDto.setPaisEnum(PaisesEnum.BR);
			
			EstadoDTO estadoDto = new EstadoDTO();
			CidadeDTO cidadeDto = new CidadeDTO();
			EnderecoDTO enderecoDto = new EnderecoDTO();
			UsuarioPessoaFisicaDTO usuarioDto = new UsuarioPessoaFisicaDTO();
			
			usuarioDto.setIpUsuario(enderecoIp);
			
//			usuarioDto.setTipoDtoUsuario(TipoDtoUsuarioEnum.USUARIO_PESSOA_FISICA);
			
			
			List<EnderecoDTO> listEnderecos = new ArrayList<EnderecoDTO>();
			
			String paisDeRetorno = retornoJson.get("countryCode").toString();
			
			if(paisDeRetorno.equals(PaisesEnum.BR.getDescricao())) {
				paisDto.setPaisEnum(PaisesEnum.BR);
			}
			
			estadoDto.setNome(retornoJson.get("region").toString().substring(0, 2));
			estadoDto.setPais(paisDto);
			
			cidadeDto.setNome(retornoJson.get("city").toString());
			cidadeDto.setEstado(estadoDto);
			
			enderecoDto = PointEndereco.inserirPointsNoEndereco(enderecoDto, retornoJson.get("lon").toString(), retornoJson.get("lat").toString());
			
			enderecoDto.setLongitude(new BigDecimal(retornoJson.get("lon").toString()));
			enderecoDto.setLatitude(new BigDecimal(retornoJson.get("lat").toString()));
			enderecoDto.setEnderecoDefault(true);
			
			enderecoDto.setCidade(cidadeDto);
			
			listEnderecos.add(enderecoDto);
			
			usuarioDto.setEnderecosList(listEnderecos);
			
			connection.disconnect();
			
			return usuarioDto;
			
		} else {
			
			logger.info("usuario retornou vazio");
			
			return null;
		
		}
	}
	
}
